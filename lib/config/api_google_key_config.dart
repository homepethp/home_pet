import 'dart:io';

class ApiGoogleKeyConfig{
  static String apiGoogleKeyDev = Platform.isAndroid
      ? 'AIzaSyDiORe6ZCRVZLx8I5tpau0-ukndKl5OTg4'
      : 'AIzaSyDQgWJNXQDX0iiObzwHds2FbYLZh70tvrk';

  static String apiGoogleKeyBeta = Platform.isAndroid
      ? 'AIzaSyC40BvWAWTitXAVAPlIP7azk2Uy7U9pQYE'
      : 'AIzaSyBHJAiwny1iksvBHuwIevrVN95uII8ur6w';

  static String apiGoogleKeyProduction = Platform.isAndroid
      ? 'AIzaSyAaLWV_UMolZzyuBLuYOKHy8fDCTgacH-g'
      : 'AIzaSyD2OSEMJe4baoEB4tj2a5kq08HdHUc0vQo';
}