import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors{
  static const bg = Color(0xfffafafa);
  static const primary = Color(0xff40acba);

  static const fontPrimary = Color(0xff003366);

  static LinearGradient primaryGradient(){
    return const LinearGradient(
        colors: [
          Color(0xFF006699),
          Color(0xFF33ccff),
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter
    );
  }
}