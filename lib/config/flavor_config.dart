enum Flavor{
  development,
  beta,
  production
}

class FlavorValues{
  final String baseURL;
  final String apiGoogleKey;
  String? baseURLAPI;
  String? baseApiGoogleKey;

  FlavorValues({
    required this.baseURL,
    required this.apiGoogleKey
  }){
    baseURLAPI = baseURL;
    baseApiGoogleKey = apiGoogleKey;

  }
}

class FlavorConfig{
  final Flavor flavor;
  final FlavorValues values;
  static FlavorConfig? _flavorConfig;

  FlavorConfig({required this.flavor, required this.values}){
    _flavorConfig = FlavorConfig._internal(flavor, values);
  }

  FlavorConfig._internal(this.flavor, this.values);
  static FlavorConfig? get instance {return _flavorConfig;}
}
