import 'package:dio/dio.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/usecases/get/get_user_token.dart';

class AppInterceptors extends Interceptor {

  final GetUserToken getUserToken;

  AppInterceptors({required this.getUserToken});

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    String? token = await getUserToken.call(NoParams());

    if (token != '') options.headers.putIfAbsent('Authorization', () => "Bearer $token");

    super.onRequest(options, handler);
  }

  @override
  Future<void> onError(DioError err, ErrorInterceptorHandler handler) async {
    return super.onError(err, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    super.onResponse(response, handler);
  }
}
