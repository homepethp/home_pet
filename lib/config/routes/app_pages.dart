import 'package:get/get.dart';
import 'package:home_pet/presentation/views/before_dashboard/forgot_password/forgot_password_enter_code_page.dart';
import 'package:home_pet/presentation/views/before_dashboard/forgot_password/forgot_password_enter_new_password.dart';
import 'package:home_pet/presentation/views/before_dashboard/forgot_password/forgot_password_page.dart';
import 'package:home_pet/presentation/views/before_dashboard/forgot_password/fprgot_password_binding.dart';
import 'package:home_pet/presentation/views/before_dashboard/login/login_binding.dart';
import 'package:home_pet/presentation/views/before_dashboard/login/login_page.dart';
import 'package:home_pet/presentation/views/before_dashboard/recommend/recommend_binding.dart';
import 'package:home_pet/presentation/views/before_dashboard/recommend/recommend_page.dart';
import 'package:home_pet/presentation/views/before_dashboard/register/register_binding.dart';
import 'package:home_pet/presentation/views/chat/chat_binding.dart';
import 'package:home_pet/presentation/views/chat/chat_page.dart';
import 'package:home_pet/presentation/views/home/home_binding.dart';
import 'package:home_pet/presentation/views/home/home_page.dart';
import 'package:home_pet/presentation/views/post/post_binding.dart';
import 'package:home_pet/presentation/views/post/post_page.dart';
import 'package:home_pet/presentation/views/reuse/map/map_binding.dart';
import 'package:home_pet/presentation/views/reuse/map/map_page.dart';
import 'package:home_pet/presentation/views/setting/setting_binding.dart';
import 'package:home_pet/presentation/views/setting/setting_controller.dart';
import 'package:home_pet/presentation/views/setting/setting_page.dart';

import '../../presentation/views/before_dashboard/register/register_page.dart';
import '../../presentation/views/dashboard/dashboard_binding.dart';
import '../../presentation/views/dashboard/dashboard_page.dart';
import '../../presentation/views/splash/splash_binding.dart';
import '../../presentation/views/splash/splash_page.dart';
import 'app_routes.dart';

class AppPages {
  static const initial = Routes.splash;

  static final routes = [
    GetPage(name: Routes.splash, page: ()=> const SplashPage(), binding: SplashBinding()),
    GetPage(name: Routes.recommend, page: ()=> const RecommendPage(), binding: RecommendBinding()),
    GetPage(name: Routes.login, page: ()=> const LoginPage(), binding: LoginBinding()),
    GetPage(name: Routes.register, page: ()=> const RegisterPage(), binding: RegisterBinding()),
    GetPage(name: Routes.forgotPassword, page: ()=> const ForgotPasswordPage(), binding: ForgotPasswordBinding()),
    GetPage(name: Routes.forgotPasswordEnterCode, page: ()=> const ForgotPasswordEnterCodePage(), binding: ForgotPasswordBinding()),
    GetPage(name: Routes.forgotPasswordEnterNewPassword, page: ()=> const ForgotPasswordEnterNewPassword(), binding: ForgotPasswordBinding()),

    GetPage(name: Routes.dashBoard, page: ()=> const DashBoardPage(), binding: DashBoardBinding()),
    GetPage(name: Routes.home, page: ()=> HomePage(), binding: HomeBinding()),
    GetPage(name: Routes.chat, page: ()=> const ChatPage(), binding: ChatBinding()),
    GetPage(name: Routes.setting, page: ()=> const SettingPage(), binding: SettingBinding()),
    GetPage(name: Routes.post, page: ()=> const PostPage(), binding: PostBinding()),
    GetPage(name: Routes.map, page: ()=> const MapPage(), binding: MapBinding()),
  ];
}