abstract class Routes {
  static const splash = '/splash';
  static const recommend = '/before_dashboard/recommend';
  static const login = '/before_dashboard/login';
  static const register = '/before_dashboard/register';
  static const forgotPassword = '/before_dashboard/forgot_password';
  static const forgotPasswordEnterCode = '/before_dashboard/forgot_password_enter_code';
  static const forgotPasswordEnterNewPassword = '/before_dashboard/forgot_password_enter_new_password';
  static const dashBoard = '/dashboard';
  static const home = '/dashboard/home';
  static const chat = '/dashboard/chat';
  static const setting = '/dashboard/setting';
  static const post = '/post';
  static const map = '/map';
}