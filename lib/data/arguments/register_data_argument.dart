import 'package:home_pet/domain/entities/data_google_profile_entity.dart';

class RegisterDataArgument{
  DataGoogleProfileEntity? dataGoogleProfileEntity;

  RegisterDataArgument({this.dataGoogleProfileEntity});
}