import 'package:home_pet/data/models/data_user_login_model.dart';

import '../../../../core/result/app_result.dart';
import '../../../request/save_user_login_request.dart';

abstract class UserAuthLocalDatasource{

  Future<void> saveUserLogin(SaveUserLoginRequest params);
  Future<void> saveSkipRecommend();

  Future<bool> getSkipRecommend();
  Future<String> getUserToken();
  Future<AppResult<DataUserLoginModel>> getDataUserLogin();
}