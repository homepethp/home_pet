
import 'dart:convert';

import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/models/data_user_login_model.dart';
import 'package:home_pet/data/request/save_user_login_request.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'data_sources/user_auth_local_datasource.dart';

class UserAuthLocal extends UserAuthLocalDatasource{
  late final _token = 'token';
  late final _dataUserLogin = 'dataUserLogin';
  late final _skipRecommend = 'skipRecommend';

  @override
  Future saveUserLogin(SaveUserLoginRequest params) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Map<String, dynamic> dataUserEncode = {
      'username': params.username,
      'password': params.password
    };

    preferences.setString(_token, params.token);
    preferences.setString(_dataUserLogin, jsonEncode(dataUserEncode));
  }

  @override
  Future<void> saveSkipRecommend() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(_skipRecommend, true);
  }

  @override
  Future<bool> getSkipRecommend() async {
    try{
      SharedPreferences preferences = await SharedPreferences.getInstance();
      bool? result = preferences.getBool(_skipRecommend);
      return result!;
    }catch(e){
      return false;
    }
  }

  @override
  Future<AppResult<DataUserLoginModel>> getDataUserLogin() async {
    try{
      SharedPreferences preferences = await SharedPreferences.getInstance();
      String? resultEncode = preferences.getString(_dataUserLogin);
      Map<String, dynamic>? resultDecode = jsonDecode(resultEncode!);

      DataUserLoginModel? result;
      if(resultDecode != null) result = DataUserLoginModel.fromJson(resultDecode);

      return Success(value: result!);
    }catch(e){
      return Error(errorObject: e);
    }
  }

  @override
  Future<String> getUserToken() async {
    try{
      SharedPreferences preferences = await SharedPreferences.getInstance();

      String? result = preferences.getString(_token);

      return result!;
    }catch(e){
      return '';
    }
  }
}