import 'package:home_pet/data/models/post_feed_model.dart';
import 'package:home_pet/data/request/get_feed_request.dart';
import 'package:home_pet/data/response/base_response.dart';

import '../../../../core/result/app_result.dart';

abstract class HomeRemoteDatasource{
  Future<AppResult<AppFeedResponse<List<PostFeedModel>>>> getPostFeed(GetFeedRequest params);
}