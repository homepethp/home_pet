import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/models/current_position_model.dart';

import '../../../../domain/entities/place_search_entity.dart';
import '../../../models/place_search_model.dart';

abstract class LocationRemoteDataSource{

  Future<AppResult<CurrentPositionModel>> getCurrentPosition();
  Future<AppResult<List<PlaceSearchModel>>> searchLocationByKeyword(String keyword);

  Future<AppResult<CurrentPositionModel>> patchPlaceSearch(PlaceSearchEntity params);
}