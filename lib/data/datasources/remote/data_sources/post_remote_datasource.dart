import 'package:home_pet/data/request/post_request.dart';

import '../../../../core/result/app_result.dart';

abstract class PostRemoteDataSource{

  Future<AppResult<void>> createPost(PostRequest params);
}