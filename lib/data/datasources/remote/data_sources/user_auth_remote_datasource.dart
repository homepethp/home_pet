import 'package:home_pet/data/models/data_google_profile_model.dart';
import 'package:home_pet/data/request/email_request.dart';
import 'package:home_pet/data/request/login_request.dart';
import 'package:home_pet/data/request/register_request.dart';

import '../../../../core/result/app_result.dart';
import '../../../models/user_auth_model.dart';
import '../../../request/login_with_google_request.dart';
import '../../../request/verify_code_request.dart';

abstract class UserAuthRemoteDataSource{
  Future<AppResult<UserAuthModel>> userRegister(RegisterRequest params);
  Future<AppResult<UserAuthModel>> userLogin(LoginRequest params);
  Future<AppResult<UserAuthModel>> userLoginWithGoogleToApi(LoginWithGoogleRequest params);
  Future<AppResult<DataGoogleProfileModel>> userLoginWithGoogle();
  Future<AppResult<bool>> forgotPasswordByEmail(EmailRequest params);
  Future<AppResult<bool>> forgotPasswordByVerifyCode(VerifyCodeRequest params);

  Future<void> signOutFromGoogle();
}