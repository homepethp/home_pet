import 'package:dio/dio.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/datasources/remote/data_sources/home_remote_datasource.dart';
import 'package:home_pet/data/models/post_feed_model.dart';
import 'package:home_pet/data/request/get_feed_request.dart';
import 'package:home_pet/data/response/base_response.dart';
import 'package:home_pet/data/response/remote/post_feed_response.dart';

class HomeRemote implements HomeRemoteDatasource{
  final Dio dio;

  HomeRemote({required this.dio});

  final String _getFeed = 'post/feed';

  @override
  Future<AppResult<AppFeedResponse<List<PostFeedModel>>>> getPostFeed(GetFeedRequest params) async {
    try{
      Response dioResponse = await dio.get('$_getFeed?page=${params.numPage}&type_pet=${params.typePet}');

      PostFeedResponse response = PostFeedResponse.fromJson(dioResponse.data);

      List<PostFeedModel> listResult = [];

      for(PostFeedItemResponse itemResponse in response.listData){
        listResult.add(PostFeedModel.fromResponse(itemResponse));
      }

      return Success(value: AppFeedResponse(lastPage: response.lastPage, numPage: response.numPage, value: listResult));
    }on DioError catch(e){
      return Error(errorObject: e);
    }
  }
}