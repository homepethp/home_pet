import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:home_pet/data/datasources/remote/data_sources/location_remote_datasource.dart';
import 'package:home_pet/data/models/place_search_model.dart';

import '../../../core/result/app_result.dart';
import '../../../domain/entities/place_search_entity.dart';
import '../../models/current_position_model.dart';
import '../../response/remote/place_search_response.dart';

class LocationRemote implements LocationRemoteDataSource{
  Dio dio;
  LocationRemote({required this.dio});

  final String googleApikey = 'AIzaSyCQtnVw8OUIe3tjdGTsGBFKg91cl78r-XI';

  @override
  Future<AppResult<CurrentPositionModel>> getCurrentPosition() async {
    try{
      final location = await Geolocator.getCurrentPosition(
          forceAndroidLocationManager: false,
          desiredAccuracy: LocationAccuracy.high,
      ).timeout(const Duration(seconds: 20));

      CurrentPositionModel? result;

      await placemarkFromCoordinates(location.latitude, location.longitude, localeIdentifier: 'th').then((List<Placemark> placemarks) {
        Placemark place = placemarks[0];

        result = CurrentPositionModel.fromResponse(place, LatLng(location.latitude, location.longitude));

        debugPrint('$placemarks');
      }).catchError((e) {
        debugPrint(e.toString());
      });

      return Success(value: result!);
    }catch(e){
      return Error(errorObject: e);
    }
  }

  @override
  Future<AppResult<List<PlaceSearchModel>>> searchLocationByKeyword(String keyword) async {
    try{
      Response dioResponse = await dio.get('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$keyword&components=country:th&language=th&key=$googleApikey');

      PlaceSearchResponse response = PlaceSearchResponse.fromJson(dioResponse.data);

      List<PlaceSearchModel> listResult = [];

      for(PlaceSearchItemResponse itemResponse in response.listPlaceSearch){
        listResult.add(PlaceSearchModel.fromResponse(itemResponse));
      }

      return Success(value: listResult);
    }on DioError catch(e){
      return Error(exception: e);
    }
  }

  @override
  Future<AppResult<CurrentPositionModel>> patchPlaceSearch(PlaceSearchEntity params) async {
    try{
      final plist = GoogleMapsPlaces(apiKey: googleApikey, apiHeaders: await const GoogleApiHeaders().getHeaders());

      String placeId = params.placeId;
      PlacesDetailsResponse detail = await plist.getDetailsByPlaceId(placeId, region: 'th', language: 'th');
      Geometry geometry = detail.result.geometry!;

      final LatLng latLng = LatLng(geometry.location.lat, geometry.location.lng);

      CurrentPositionModel? result;

      await placemarkFromCoordinates(latLng.latitude, latLng.longitude, localeIdentifier: 'th').then((List<Placemark> placemarks) {
        Placemark place = placemarks[0];

        result = CurrentPositionModel.fromResponse(place, LatLng(latLng.latitude, latLng.longitude), namePlaceSearch: detail.result.formattedAddress);

        debugPrint('$placemarks');
      }).catchError((e) {
        debugPrint(e.toString());
      });

      return Success(value: result!);
    }catch(e){
      return Error(errorObject: e);
    }
  }
}