import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/datasources/remote/data_sources/post_remote_datasource.dart';
import 'package:home_pet/data/request/post_request.dart';

class PostRemote implements PostRemoteDataSource{
  final Dio dio;

  PostRemote({required this.dio});

  final String _createPost = 'post/create';

  @override
  Future<AppResult<void>> createPost(PostRequest params) async {
    try{
      List<dynamic> listDataFile = [];

      for(File item in params.listPicture){
        String fileName = item.path.split('/').last;
        listDataFile.add(MultipartFile.fromFileSync(item.path, filename:fileName, contentType: MediaType('image', fileName.split('.').last)));
      }

      FormData formData = FormData.fromMap({
        'file': listDataFile.toList(),
        'description' : params.description,
        'sex' : params.sex,
        'age' : params.age,
        'type_pet' : params.typePet,
        'latitude' : params.location.latLng.latitude.toString(),
        'longitude' : params.location.latLng.longitude.toString(),
        'address' : params.location.addressName,
        'sub_district' : params.location.subDistrict,
        'district' : params.location.district,
        'province' : params.location.province
      });

      dio.options.headers['Content-Type'] = 'multipart/form-data';

      Response dioResponse = await dio.post(_createPost, data: formData);

      return Success(value: null);
    }on DioError catch(e){
      return Error(errorObject: e);
    }
  }
}