import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:home_pet/core/result/app_event_result.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/models/data_google_profile_model.dart';

import 'package:home_pet/data/models/user_auth_model.dart';
import 'package:home_pet/data/request/email_request.dart';
import 'package:home_pet/data/request/login_request.dart';

import 'package:home_pet/data/request/register_request.dart';
import 'package:home_pet/data/request/verify_code_request.dart';
import 'package:home_pet/data/response/base_response.dart';
import 'package:home_pet/data/response/remote/login_response.dart';
import 'package:home_pet/data/response/remote/register_response.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../../../firebase_options.dart';
import '../../request/login_with_google_request.dart';
import '../../response/remote/google_profile_response.dart';
import 'data_sources/user_auth_remote_datasource.dart';

class UserAuthRemote implements UserAuthRemoteDataSource{
  final Dio dio;
  UserAuthRemote({required this.dio});

  final String _userRegister = 'user/register';
  final String _userLogin = 'user/login';
  final String _userLoginWithGoogle = 'user/login/google';
  final String _userForgotPassword = 'user/forgot_password';
  final String _userForgotPasswordByVerifyCode = 'user/verify_forgot_password';

  final GoogleSignIn _googleSignIn = Platform.isIOS
      ? GoogleSignIn(clientId: DefaultFirebaseOptions.currentPlatform.iosClientId)
      : GoogleSignIn();

  final SignInWithApple _appleSignIn = SignInWithApple();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Future<AppResult<UserAuthModel>> userRegister(RegisterRequest params) async {
    try{
      String fileName = params.fileImage.path.split('/').last;
      FormData formData = FormData.fromMap({
        'avatar': await MultipartFile.fromFile(params.fileImage.path, filename:fileName, contentType: MediaType('image', fileName.split('.').last)),
        'name_account': params.nameAccount,
        'username': params.username,
        'password': params.password,
        'email': params.email,
        'phone': params.phone,
        'apple_id': params.appleId,
        'google_avatar': params.googleAvatar,
        'google_id': params.googleId,
        'google_id_token': params.googleIdToken
      });

      dio.options.headers['Content-Type'] = 'multipart/form-data';

      Response dioResponse = await dio.post(_userRegister, data: formData);

      RegisterResponse response = RegisterResponse.fromJson(dioResponse.data);

      UserAuthModel? result;

      if(response.resultCode == AppResponseStatus.SUCCESS){
        result = UserAuthModel.fromRegisterResponse(response);
      }

      return Success(value: result!);
    }on DioError catch(e){
      return Error(exception: e, errorObject: BaseResponseError.fromJson(e.response?.data));
    }
  }

  @override
  Future<AppResult<UserAuthModel>> userLogin(LoginRequest params) async {
    try{
      Map<String, dynamic> dataBody = {
        'username': params.username,
        'password': params.password
      };

      Response dioResponse = await dio.post(_userLogin, data: dataBody);

      LoginResponse response = LoginResponse.fromJson(dioResponse.data);

      UserAuthModel? result;

      if(response.resultCode == AppResponseStatus.SUCCESS){
        result = UserAuthModel.fromLoginResponse(response);
      }

      return Success(value: result!);
    }on DioError catch(e){
      return Error(exception: e, errorObject: BaseResponseError.fromJson(e.response?.data));
    }
  }

  @override
  Future<AppResult<DataGoogleProfileModel>> userLoginWithGoogle() async {
    try{
      final GoogleSignInAccount? googleSignInAccount = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount!.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken
      );

      UserCredential userCredential = await _firebaseAuth.signInWithCredential(credential);

      Map<String, dynamic>? resultData = userCredential.additionalUserInfo?.profile;
      resultData?.putIfAbsent('uid', () => userCredential.user?.providerData.first.uid);
      resultData?.putIfAbsent('id_token', () => googleSignInAuthentication.idToken);

      GoogleProfileResponse response = GoogleProfileResponse.fromJson(resultData!);
      DataGoogleProfileModel result = DataGoogleProfileModel.fromResponse(response);

      return Success(value: result);
    }catch(e){
      return Error(errorObject: e);
    }
  }

  @override
  Future<AppResult<UserAuthModel>> userLoginWithGoogleToApi(LoginWithGoogleRequest params) async {
    try{
      Map<String, dynamic> dataBody = {
        'google_id': params.googleId,
      };

      Response dioResponse = await dio.post(_userLoginWithGoogle, data: dataBody);

      LoginResponse response = LoginResponse.fromJson(dioResponse.data);

      UserAuthModel? result;

      if(response.resultCode == AppResponseStatus.SUCCESS){
        result = UserAuthModel.fromLoginResponse(response);
      }

      return Success(value: result!);
    }on DioError catch(e){
      return Error(exception: e, errorObject: BaseResponseError.fromJson(e.response?.data));
    }
  }

  @override
  Future<AppResult<bool>> forgotPasswordByEmail(EmailRequest params) async {
    try{
      Map<String, dynamic> dataBody = {
        'email': params.email
      };

      Response dioResponse = await dio.post(_userForgotPassword, data: dataBody);

      BaseResponse response = BaseResponse.fromJson(dioResponse.data);

      bool? result;

      if(response.resultCode == AppResponseStatus.SUCCESS){
        result = response.isSuccess;
      }

      return Success(value: result!);
    }on DioError catch(e){
      return Error(exception: e, errorObject: BaseResponseError.fromJson(e.response?.data));
    }
  }

  @override
  Future<AppResult<bool>> forgotPasswordByVerifyCode(VerifyCodeRequest params) async {
    try{
      Map<String, dynamic> dataBody = {
        'code': params.code,
        'email': params.email
      };

      Response dioResponse = await dio.post(_userForgotPasswordByVerifyCode, data: dataBody);

      BaseResponse response = BaseResponse.fromJson(dioResponse.data);

      bool? result;

      if(response.resultCode == AppResponseStatus.SUCCESS){
        result = response.isSuccess;
      }

      return Success(value: result!);
    }on DioError catch(e){
      return Error(exception: e, errorObject: BaseResponseError.fromJson(e.response?.data));
    }
  }

  @override
  Future<void> signOutFromGoogle() async {
    await _googleSignIn.signOut();
    await _firebaseAuth.signOut();
  }
}