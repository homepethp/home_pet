import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../domain/entities/current_position_entity.dart';

class CurrentPositionModel extends CurrentPositionEntity{
  CurrentPositionModel({
    required super.addressName,
    required super.province,
    required super.district,
    required super.subDistrict,
    required super.latLng
  });

  factory CurrentPositionModel.fromResponse(Placemark itemResponse, LatLng latLng, {String? namePlaceSearch})=> CurrentPositionModel(
      addressName: namePlaceSearch ?? itemResponse.street!,
      province: itemResponse.locality!,
      district: itemResponse.subAdministrativeArea!,
      subDistrict: itemResponse.subLocality!,
      latLng: latLng
  );
}