import 'package:home_pet/data/response/remote/google_profile_response.dart';
import 'package:home_pet/domain/entities/data_google_profile_entity.dart';

class DataGoogleProfileModel extends DataGoogleProfileEntity{

  DataGoogleProfileModel({
    required super.id,
    required super.idToken,
    required super.name,
    required super.firstName,
    required super.lastName,
    required super.email,
    required super.imageURL
  });

  factory DataGoogleProfileModel.fromResponse(GoogleProfileResponse itemResponse)=> DataGoogleProfileModel(
      id: itemResponse.id,
      idToken: itemResponse.idToken,
      name: itemResponse.name,
      firstName: itemResponse.firstName,
      lastName: itemResponse.lastName,
      email: itemResponse.email,
      imageURL: itemResponse.imageURL
  );
}


