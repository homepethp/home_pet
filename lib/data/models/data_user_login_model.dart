import 'package:home_pet/domain/entities/data_user_login_entity.dart';

class DataUserLoginModel extends DataUserLoginEntity {

  DataUserLoginModel({
    required super.username,
    required super.password
  });

  factory DataUserLoginModel.fromJson(Map<String, dynamic> parsedJson)=> DataUserLoginModel(
      username: parsedJson['username'],
      password: parsedJson['password']
  );
}