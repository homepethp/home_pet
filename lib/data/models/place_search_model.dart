import 'package:home_pet/data/response/remote/place_search_response.dart';
import 'package:home_pet/domain/entities/place_search_entity.dart';

class PlaceSearchModel extends PlaceSearchEntity{

  PlaceSearchModel({
    required super.addressName,
    required super.placeId
  });

  factory PlaceSearchModel.fromResponse(PlaceSearchItemResponse itemResponse)=> PlaceSearchModel(
      addressName: itemResponse.addressName,
      placeId: itemResponse.placeId
  );
}