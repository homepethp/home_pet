import 'package:home_pet/data/response/remote/post_feed_response.dart';
import 'package:home_pet/domain/entities/post_feed_entity.dart';

import '../../domain/entities/file_image_pet_entity.dart';
import '../response/remote/file_image_pet_response.dart';

class PostFeedModel extends PostFeedEntity{
  PostFeedModel({
    required super.postId,
    required super.userPostId,
    required super.countLike,
    required super.createAt,
    required super.lastTime,
    required super.date,
    required super.agePet,
    required super.statusPostId,
    required super.sexPetId,
    required super.typePetId,
    required super.updateAt,
    required super.updateBy,
    required super.userPost,
    required super.listImagePet
  });

  factory PostFeedModel.fromResponse(PostFeedItemResponse itemResponse)=> PostFeedModel(
      postId: itemResponse.postId,
      userPostId: itemResponse.userPostId,
      countLike: itemResponse.countLike,
      createAt: itemResponse.createAt,
      lastTime: itemResponse.lastTime,
      date: itemResponse.date,
      agePet: itemResponse.agePet,
      statusPostId: itemResponse.statusPostId,
      sexPetId: itemResponse.sexPetId,
      typePetId: itemResponse.typePetId,
      updateAt: itemResponse.updateAt,
      updateBy: itemResponse.updateBy,
      userPost: UserPostEntity(avatar: itemResponse.userPost.avatar, nameAccount: itemResponse.userPost.nameAccount),
      listImagePet: [
          for(FileImagePetResponse item in itemResponse.listImagePet)
            FileImagePetEntity(fileId: item.fileId, path: item.path)
      ]
  );
}