import 'package:get/get.dart';

class SlideRecommendModel {
  final String imageHead;
  final String imageBody;
  final String title;
  final String description;

  SlideRecommendModel({
    required this.imageHead,
    required this.imageBody,
    required this.title,
    required this.description,
  });
}

List<SlideRecommendModel> get slideList => _slideList;

List<SlideRecommendModel> _slideList = [
  SlideRecommendModel(
    imageHead: 'assets/png/bg_head_recommend_1.png',
    imageBody: 'assets/png/bg_body_recommend_1.png',
    title: 'title_recommend_first'.tr,
    description: 'description_recommend_first'.tr,
  ),
  SlideRecommendModel(
    imageHead: 'assets/png/bg_head_recommend_2.png',
    imageBody: 'assets/png/bg_body_recommend_2.png',
    title: 'title_recommend_second'.tr,
    description: 'description_recommend_second'.tr,
  ),
];


