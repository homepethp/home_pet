import 'package:home_pet/data/request/register_request.dart';
import 'package:home_pet/data/response/remote/login_response.dart';
import 'package:home_pet/data/response/remote/register_response.dart';
import 'package:home_pet/domain/entities/user_auth_entity.dart';

class UserAuthModel extends UserAuthEntity{

  UserAuthModel({
    required super.id,
    required super.role,
    required super.token,
    required super.userName,
  });

  factory UserAuthModel.fromRegisterResponse(RegisterResponse itemResponse) => UserAuthModel(
      id: itemResponse.id,
      role: itemResponse.role,
      token: itemResponse.token,
      userName: itemResponse.username,
  );

  factory UserAuthModel.fromLoginResponse(LoginResponse itemResponse) => UserAuthModel(
    id: itemResponse.id,
    role: itemResponse.role,
    token: itemResponse.token,
    userName: itemResponse.username,
  );
}