import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/datasources/remote/data_sources/home_remote_datasource.dart';
import 'package:home_pet/data/request/get_feed_request.dart';
import 'package:home_pet/data/response/base_response.dart';
import 'package:home_pet/domain/entities/post_feed_entity.dart';
import 'package:home_pet/domain/repositories/home_repository.dart';

class HomeRepositoryImpl implements HomeRepository{
  final HomeRemoteDatasource homeRemoteDatasource;

  HomeRepositoryImpl({required this.homeRemoteDatasource});

  @override
  Future<AppResult<AppFeedResponse<List<PostFeedEntity>>>> getPostFeed(GetFeedRequest params) {
    return homeRemoteDatasource.getPostFeed(params);
  }
}