import 'package:home_pet/data/datasources/remote/data_sources/location_remote_datasource.dart';
import 'package:home_pet/domain/repositories/location_repository.dart';

import '../../core/result/app_result.dart';
import '../../domain/entities/current_position_entity.dart';
import '../../domain/entities/place_search_entity.dart';

class LocationRepositoryImpl implements LocationRepository{
  final LocationRemoteDataSource locationRemoteDatasource;

  LocationRepositoryImpl({required this.locationRemoteDatasource});

  @override
  Future<AppResult<CurrentPositionEntity>> getCurrentPosition() {
    return locationRemoteDatasource.getCurrentPosition();
  }

  @override
  Future<AppResult<List<PlaceSearchEntity>>> searchLocationByKeyword(String keyword) {
    return locationRemoteDatasource.searchLocationByKeyword(keyword);
  }

  @override
  Future<AppResult<CurrentPositionEntity>> patchPlaceSearch(PlaceSearchEntity params) {
    return locationRemoteDatasource.patchPlaceSearch(params);
  }
}