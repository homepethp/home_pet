import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/datasources/remote/data_sources/post_remote_datasource.dart';
import 'package:home_pet/data/request/post_request.dart';
import 'package:home_pet/domain/repositories/post_repository.dart';

class PostRepositoryImpl implements PostRepository{
  final PostRemoteDataSource postRemoteDataSource;

  PostRepositoryImpl({required this.postRemoteDataSource});

  @override
  Future<AppResult<void>> createPost(PostRequest params) {
    return postRemoteDataSource.createPost(params);
  }
}