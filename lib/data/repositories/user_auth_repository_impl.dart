import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/datasources/remote/data_sources/user_auth_remote_datasource.dart';
import 'package:home_pet/data/request/email_request.dart';
import 'package:home_pet/data/request/login_request.dart';
import 'package:home_pet/data/request/login_with_google_request.dart';
import 'package:home_pet/data/request/register_request.dart';
import 'package:home_pet/data/request/verify_code_request.dart';
import 'package:home_pet/domain/entities/data_google_profile_entity.dart';
import 'package:home_pet/domain/entities/data_user_login_entity.dart';
import 'package:home_pet/domain/entities/user_auth_entity.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

import '../datasources/local/data_sources/user_auth_local_datasource.dart';
import '../request/save_user_login_request.dart';

class UserAuthRepositoryImpl implements UserAuthRepository{
  final UserAuthRemoteDataSource userAuthRemoteDataSource;
  final UserAuthLocalDatasource userAuthLocalDataSource;

  UserAuthRepositoryImpl({required this.userAuthRemoteDataSource, required this.userAuthLocalDataSource});

  @override
  Future<AppResult<UserAuthEntity>> userRegister(RegisterRequest params) async {
    final userAuthEntity = await userAuthRemoteDataSource.userRegister(params);
    return userAuthEntity;
  }

  @override
  Future<void> saveUserLogin(SaveUserLoginRequest params) {
    return userAuthLocalDataSource.saveUserLogin(params);
  }

  @override
  Future<void> saveSkipRecommend() {
    return userAuthLocalDataSource.saveSkipRecommend();
  }

  @override
  Future<bool> getSkipRecommend() {
    return userAuthLocalDataSource.getSkipRecommend();
  }

  @override
  Future<String> getUserToken() {
    return userAuthLocalDataSource.getUserToken();
  }

  @override
  Future<AppResult<UserAuthEntity>> userLogin(LoginRequest params) {
    return userAuthRemoteDataSource.userLogin(params);
  }

  @override
  Future<AppResult<UserAuthEntity>> userLoginWithGoogleToApi(LoginWithGoogleRequest params) {
    return userAuthRemoteDataSource.userLoginWithGoogleToApi(params);
  }

  @override
  Future<AppResult<bool>> forgotPasswordByEmail(EmailRequest params) {
    return userAuthRemoteDataSource.forgotPasswordByEmail(params);
  }

  @override
  Future<AppResult<bool>> forgotPasswordByVerifyCode(VerifyCodeRequest params) {
    return userAuthRemoteDataSource.forgotPasswordByVerifyCode(params);
  }

  @override
  Future<AppResult<DataUserLoginEntity>> getDataUserLogin() async {
    final dataUserLoginEntity = await userAuthLocalDataSource.getDataUserLogin();
    return dataUserLoginEntity;
  }

  @override
  Future<AppResult<DataGoogleProfileEntity>> userLoginWithGoogle() async {
    final dataGoogleProfileEntity = await userAuthRemoteDataSource.userLoginWithGoogle();
    return dataGoogleProfileEntity;
  }

  @override
  Future<void> signOutFromGoogle() {
    return userAuthRemoteDataSource.signOutFromGoogle();
  }
}