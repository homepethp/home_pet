class GetFeedRequest{
  final int numPage;
  final String typePet;

  GetFeedRequest({required this.numPage, required this.typePet});
}