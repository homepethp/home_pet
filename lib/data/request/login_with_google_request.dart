class LoginWithGoogleRequest{
  final String googleId;

  LoginWithGoogleRequest({required this.googleId});
}