import 'dart:io';

import 'package:home_pet/domain/entities/current_position_entity.dart';

class PostRequest{
  final List<File> listPicture;
  final String description;
  final int sex;
  final String age;
  final int typePet;
  final CurrentPositionEntity location;

  PostRequest({required this.listPicture, required this.description, required this.sex, required this.age, required this.typePet, required this.location});
}