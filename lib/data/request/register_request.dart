import 'dart:io';

class RegisterRequest{
  final File fileImage;
  final String nameAccount;
  final String username;
  final String password;
  final String email;
  final String phone;
  String? googleAvatar;
  String? googleId;
  String? googleIdToken;
  String? appleId;

  RegisterRequest({
    required this.fileImage,
    required this.nameAccount,
    required this.username,
    required this.password,
    required this.email,
    required this.phone,
    this.googleAvatar,
    this.googleId,
    this.googleIdToken,
    this.appleId
  });
}