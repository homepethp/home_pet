class SaveUserLoginRequest{
  final String username;
  final String password;
  final String token;

  SaveUserLoginRequest({required this.username, required this.password, required this.token});
}