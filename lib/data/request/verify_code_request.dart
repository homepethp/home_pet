import 'package:home_pet/data/request/email_request.dart';

class VerifyCodeRequest{
  final String code;
  final String email;

  VerifyCodeRequest({required this.code, required this.email});
}