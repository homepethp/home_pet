class FileImagePetResponse{
  final String fileId;
  final String path;

  FileImagePetResponse({required this.fileId, required this.path});
}