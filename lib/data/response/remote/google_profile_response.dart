class GoogleProfileResponse{
  late final String id;
  late final String idToken;
  late final String name;
  late final String firstName;
  late final String lastName;
  late final String email;
  late final String imageURL;

  GoogleProfileResponse.fromJson(Map<String, dynamic> parsedJson){
    id = parsedJson['uid'];
    idToken = parsedJson['id_token'];
    name = parsedJson['name'];
    firstName = parsedJson['given_name'];
    lastName = parsedJson['family_name'];
    email = parsedJson['email'];
    imageURL = parsedJson['picture'];
  }
}