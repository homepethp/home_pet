class PlaceSearchResponse {
  late final List<PlaceSearchItemResponse> listPlaceSearch;

  PlaceSearchResponse.fromJson(Map<String, dynamic> parsedJson){
    listPlaceSearch = [];

    for(Map<String, dynamic> item in parsedJson['predictions']){
      listPlaceSearch.add(PlaceSearchItemResponse.fromJson(item));
    }
  }
}

class PlaceSearchItemResponse{
  late final String addressName;
  late final String placeId;

  PlaceSearchItemResponse.fromJson(Map<String, dynamic> parsedJson){
    addressName = parsedJson['description'];
    placeId = parsedJson['place_id'];
  }
}