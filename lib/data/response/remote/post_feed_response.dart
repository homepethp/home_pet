import 'package:home_pet/data/response/base_response.dart';

import 'file_image_pet_response.dart';

class PostFeedResponse extends BaseResponse{
  late final bool lastPage;
  late final int numPage;
  late final List<PostFeedItemResponse> listData;

  PostFeedResponse.fromJson(Map<String, dynamic> parsedJson) : super.fromJson(parsedJson){
    Map<String, dynamic> data = parsedJson['data'];
    lastPage = data['last_page'];
    numPage = data['page'];

    listData = [];
    for(Map<String, dynamic> item in data['rows']){
      listData.add(PostFeedItemResponse.fromJson(item));
    }
  }
}

///--------------------------------PostFeedItemResponse----------------------------------------///

class PostFeedItemResponse{
  late final String postId;
  late final String userPostId;
  late final String countLike;
  late final String createAt;
  late final String lastTime;
  late final String date;
  late final String agePet;
  late final int statusPostId;
  late final int sexPetId;
  late final int typePetId;
  late final String updateAt;
  late final String updateBy;
  late final UserPostResponse userPost;
  late final List<FileImagePetResponse> listImagePet;

  PostFeedItemResponse.fromJson(Map<String, dynamic> parsedJson){
    postId = parsedJson['id'];
    userPostId = parsedJson['user_id'];
    countLike = parsedJson['count_like'];
    createAt = parsedJson['created_at'];
    lastTime = parsedJson['last_time'];
    date = parsedJson['date'];
    agePet = parsedJson['age'];
    statusPostId = parsedJson['status'];
    sexPetId = parsedJson['sex'];
    typePetId = parsedJson['type_pet'];
    updateAt = parsedJson['updated_at'];
    updateBy = parsedJson['updated_by'];

    Map<String, dynamic> userPostResponse = parsedJson['user_post'];
    userPost = UserPostResponse(avatar: userPostResponse['avatar'], nameAccount: userPostResponse['name_account']);

    listImagePet = [];
    for(Map<String, dynamic> item in parsedJson['file']){
      listImagePet.add(FileImagePetResponse(fileId: item['file_id'], path: item['path']));
    }
  }
}

///--------------------------------UserPostOnFeed----------------------------------------///

class UserPostResponse{
  final String avatar;
  final String nameAccount;

  UserPostResponse({required this.avatar, required this.nameAccount});
}