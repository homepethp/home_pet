import '../base_response.dart';

class RegisterResponse extends BaseResponse{
  late final String id;
  late final String role;
  late final String token;
  late final String username;


  RegisterResponse.fromJson(Map<String, dynamic> parsedJson) : super.fromJson(parsedJson){
    Map<String, dynamic> data = parsedJson['data'];
    id = data['id'];
    role = data['role'];
    token = data['token'];
    username = data['username'];
  }

}