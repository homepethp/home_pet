// ignore_for_file: avoid_print

import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:home_pet/data/datasources/local/data_sources/user_auth_local_datasource.dart';
import 'package:home_pet/data/datasources/local/user_auth_local.dart';
import 'package:home_pet/data/datasources/remote/data_sources/home_remote_datasource.dart';
import 'package:home_pet/data/datasources/remote/data_sources/location_remote_datasource.dart';
import 'package:home_pet/data/datasources/remote/home_remote.dart';
import 'package:home_pet/data/datasources/remote/post_remote.dart';
import 'package:home_pet/data/datasources/remote/data_sources/post_remote_datasource.dart';
import 'package:home_pet/data/datasources/remote/location_remote.dart';
import 'package:home_pet/data/datasources/remote/user_auth_remote.dart';
import 'package:home_pet/data/datasources/remote/data_sources/user_auth_remote_datasource.dart';
import 'package:home_pet/data/repositories/home_repository_impl.dart';
import 'package:home_pet/data/repositories/location_repository_impl.dart';
import 'package:home_pet/data/repositories/post_repository_impl.dart';
import 'package:home_pet/data/repositories/user_auth_repository_impl.dart';
import 'package:home_pet/domain/entities/base_pets_entity.dart';
import 'package:home_pet/domain/repositories/home_repository.dart';
import 'package:home_pet/domain/repositories/location_repository.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';
import 'package:home_pet/domain/usecases/get/get_current_location.dart';
import 'package:home_pet/domain/usecases/get/get_location_by_keyword.dart';
import 'package:home_pet/domain/usecases/get/get_login_with_google.dart';
import 'package:home_pet/domain/usecases/get/get_post_feed.dart';
import 'package:home_pet/domain/usecases/get/get_user_token.dart';
import 'package:home_pet/domain/usecases/local/get_data_user_login.dart';
import 'package:home_pet/domain/usecases/local/get_skip_recommend.dart';
import 'package:home_pet/domain/usecases/local/save_data_user_login.dart';
import 'package:home_pet/domain/usecases/local/save_skip_recommend.dart';
import 'package:home_pet/domain/usecases/patch/patch_place_search.dart';
import 'package:home_pet/domain/usecases/post/post_create_post.dart';
import 'package:home_pet/domain/usecases/post/post_forgot_password_by_email.dart';
import 'package:home_pet/domain/usecases/post/post_forgot_password_by_verify_code.dart';
import 'package:home_pet/domain/usecases/post/post_logout_from_google.dart';
import 'package:home_pet/domain/usecases/post/post_user_login.dart';
import 'package:home_pet/domain/usecases/post/post_user_login_with_google.dart';
import 'package:home_pet/domain/usecases/post/post_user_register.dart';
import 'package:home_pet/utils/app_input_utils.dart';

import '../config/flavor_config.dart';
import '../config/network/app_interceptor.dart';
import '../domain/repositories/post_repository.dart';

class Injector {

  Injector._constructor();
  static Injector get instance => Injector._constructor();

  void dependencies() {
    _registerRepositories();
    _registerDataSources();
    _registerUseCases();
    _registerUtils();
    _registerNetworkDependency();
  }

  late final Dio _dio = Dio();

  _registerNetworkDependency() async {
    try{
      Get.lazyPut<Dio>(() => _getNetworkFramework(FlavorConfig.instance!.values.baseURLAPI!), fenix: true);
    }catch(e){
      print(e);
    }
  }

  _registerRepositories(){
    try{
      Get.lazyPut<UserAuthRepository>(() => UserAuthRepositoryImpl(userAuthRemoteDataSource: Get.find<UserAuthRemoteDataSource>(), userAuthLocalDataSource: Get.find<UserAuthLocalDatasource>()), fenix: true);
      Get.lazyPut<LocationRepository>(() => LocationRepositoryImpl(locationRemoteDatasource: Get.find<LocationRemoteDataSource>()), fenix: true);
      Get.lazyPut<HomeRepository>(() => HomeRepositoryImpl(homeRemoteDatasource:  Get.find<HomeRemoteDatasource>()), fenix: true);
      Get.lazyPut<PostRepository>(() => PostRepositoryImpl(postRemoteDataSource: Get.find<PostRemoteDataSource>()), fenix: true);
    }catch(e){
      print(e);
    }
  }

  _registerDataSources(){
    try{
      //local
      Get.lazyPut<UserAuthLocalDatasource>(() => UserAuthLocal(), fenix: true);
      Get.lazyPut<BasePetsEntity>(() => BasePetsEntity(), fenix: true);
      //remote
      Get.lazyPut<UserAuthRemoteDataSource>(() => UserAuthRemote(dio: Get.put(_dio)), fenix: true);
      Get.lazyPut<LocationRemoteDataSource>(() => LocationRemote(dio: Get.put(_dio)), fenix: true);
      Get.lazyPut<HomeRemoteDatasource>(() => HomeRemote(dio: Get.put(_dio)), fenix: true);
      Get.lazyPut<PostRemoteDataSource>(() => PostRemote(dio: Get.put(_dio)), fenix: true);
    }catch(e){
      print(e);
    }
  }

  _registerUseCases(){
    try{
      //local
      Get.lazyPut<SaveSkipRecommend>(() => SaveSkipRecommend(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<GetSkipRecommend>(() => GetSkipRecommend(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<SaveDataUserLogin>(() => SaveDataUserLogin(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<GetDataUserLogin>(() => GetDataUserLogin(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      //post
      Get.lazyPut<PostUserLogin>(() => PostUserLogin(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<PostUserLoginWithGoogle>(() => PostUserLoginWithGoogle(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<PostUserRegister>(() => PostUserRegister(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<PostForgotPasswordByEmail>(() => PostForgotPasswordByEmail(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<PostForgotPasswordByVerifyCode>(() => PostForgotPasswordByVerifyCode(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<PostLogoutFromGoogle>(() => PostLogoutFromGoogle(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<PostCreatePost>(() => PostCreatePost(postRepository: Get.find<PostRepository>()), fenix: true);
      //get
      Get.lazyPut<GetUserToken>(() => GetUserToken(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<GetLoginWithGoogle>(() => GetLoginWithGoogle(userAuthRepository: Get.find<UserAuthRepository>()), fenix: true);
      Get.lazyPut<GetPostFeed>(() => GetPostFeed(homeRepository:  Get.find<HomeRepository>()), fenix: true);
      Get.lazyPut<GetCurrentLocation>(() => GetCurrentLocation(locationRepository: Get.find<LocationRepository>()), fenix: true);
      Get.lazyPut<GetLocationByKeyword>(() => GetLocationByKeyword(locationRepository: Get.find<LocationRepository>()), fenix: true);
      //patch
      Get.lazyPut<PatchPlaceSearch>(() => PatchPlaceSearch(locationRepository: Get.find<LocationRepository>()), fenix: true);
    }catch(e){
      print(e);
    }
  }

  _registerUtils(){
    try{
      Get.lazyPut<AppInputUtils>(() => AppInputUtils(), fenix: true);
    }catch(e){
      print(e);
    }
  }

  Dio _getNetworkFramework(String baseURL){
    _dio.options = BaseOptions(
        baseUrl: baseURL,
        connectTimeout: 60000,
        receiveTimeout: 60000
    );

    Timer(const Duration(milliseconds: 300), (){
      _dio.interceptors.add(AppInterceptors(
          getUserToken: Get.find<GetUserToken>()
      ));
    });

    if(kDebugMode){
      _dio.interceptors.add(
          LogInterceptor(responseBody: true, requestBody: true)
      );
    }

    return _dio;
  }
}