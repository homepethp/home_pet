import 'package:get/get.dart';
import 'package:home_pet/config/assets_config.dart';

class BasePetsEntity{

  BasePetsEntity get instant => BasePetsEntity();

  List<PetTypeDataEntity> get listPetType => _listPetType;
  List<String> get listAgePet => _listAgePet;
  List<PetSexDataEntity> get listSexPet => _listSexPet;

  final List<PetTypeDataEntity> _listPetType = [
    PetTypeDataEntity(typeId: 1, typeName: 'txt_cat'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_cat.png'),
    PetTypeDataEntity(typeId: 2, typeName: 'txt_dog'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_dog.png'),
    PetTypeDataEntity(typeId: 3, typeName: 'txt_hamster'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_rat.png'),
    PetTypeDataEntity(typeId: 4, typeName: 'txt_rabbit'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_rabbit.png'),
    PetTypeDataEntity(typeId: 5, typeName: 'txt_squirrel'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_squirrel.png'),
    PetTypeDataEntity(typeId: 6, typeName: 'txt_sugar_glider'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_sugarglider.png'),
    PetTypeDataEntity(typeId: 7, typeName: 'txt_snake'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_snake.png'),
    PetTypeDataEntity(typeId: 8, typeName: 'txt_monkey'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_monkey.png'),
    PetTypeDataEntity(typeId: 9, typeName: 'txt_monkey'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_monkey.png'),
    PetTypeDataEntity(typeId: 10, typeName: 'txt_hedgehog'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_dwarfhedgehog.png'),
    PetTypeDataEntity(typeId: 11, typeName: 'txt_turtle'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_turtle.png'),
    PetTypeDataEntity(typeId: 12, typeName: 'txt_chinchilla'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_squirrel.png'),
    PetTypeDataEntity(typeId: 13, typeName: 'txt_chameleon'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_chameleon.png'),
    PetTypeDataEntity(typeId: 14, typeName: 'txt_chicken_or_duck'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_chicken.png'),
    PetTypeDataEntity(typeId: 15, typeName: 'txt_fish'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_fish.png'),
    PetTypeDataEntity(typeId: 16, typeName: 'txt_pig'.tr, pathImageType: '${AssetsConfig.pathAssets}ic_pig.png'),
  ];

  final List<String> _listAgePet = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12',
  ];

  final List<PetSexDataEntity> _listSexPet = [
    PetSexDataEntity(sexId: 0, nameSex: 'txt_male'.tr),
    PetSexDataEntity(sexId: 1, nameSex: 'txt_female'.tr)
  ];
}

class PetTypeDataEntity{
  final int typeId;
  final String typeName;
  final String pathImageType;

  PetTypeDataEntity({required this.typeId, required this.typeName, required this.pathImageType});
}

class PetSexDataEntity{
  final int sexId;
  final String nameSex;

  PetSexDataEntity({required this.sexId, required this.nameSex});
}