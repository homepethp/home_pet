import 'package:home_pet/domain/entities/base_topic_entity.dart';

import '../../data/enum/app_status_post.dart';

class BasePostFeedEntity extends BaseTopicEntity{
  late final int _statusPostId;
  late final AppStatusPost _appStatusPost;

  AppStatusPost get appStatusPost => _appStatusPost;

  BasePostFeedEntity(
      int statusPostId,
      super.sexPetId,
      super.typePetId
  ) {
    _statusPostId = statusPostId;
    _appStatusPost = _getStatusPost()!;
  }

  AppStatusPost? _getStatusPost(){
    switch(_statusPostId){
      case 0 : return AppStatusPost.active;
      case 1 : return AppStatusPost.noActive;
      default: return null;
    }
  }
}