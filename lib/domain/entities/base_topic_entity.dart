import 'package:home_pet/data/enum/app_sex_pet.dart';

class BaseTopicEntity{
  late final int _sexPetId;
  late final int _typePetId;

  late final String _nameTypePet;
  late final AppSexPet _appSexPet;

  String get nameTypePet => _nameTypePet;
  AppSexPet get appSexPet => _appSexPet;

  BaseTopicEntity(
     int sexPetId,
     int typePetId
  ){
    _sexPetId = sexPetId;
    _typePetId = typePetId;

    _nameTypePet = _getNameTypePet()!;
    _appSexPet = _getSexPet()!;
  }

  String? _getNameTypePet(){
    switch(_typePetId){
      case 1 : return 'Cat';
      case 2 : return 'Dog';
      case 3 : return 'Rat';
      case 4 : return 'Rabbit';
      case 5 : return 'Squirrel';
      case 6 : return 'Sugar Rider';
      case 7 : return 'Snake';
      case 8 : return 'Monkey';
      case 9 : return 'Dwarf Hedgehog';
      case 10 : return 'Turtle';
      case 11 : return 'Chinchilla';
      case 12 : return 'Bird';
      case 13 : return 'Chameleon';
      case 14 : return 'Chicken/Duck';
      case 15 : return 'Fish';
      case 16 : return 'Pig';
      default: return null;
    }
  }

  AppSexPet? _getSexPet(){
    switch(_sexPetId){
      case 0 : return AppSexPet.male;
      case 1 : return AppSexPet.feMale;
      default: return null;
    }
  }
}