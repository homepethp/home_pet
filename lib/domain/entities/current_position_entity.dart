import 'package:google_maps_flutter/google_maps_flutter.dart';

class CurrentPositionEntity{
  final String addressName;
  final String province;
  final String district;
  final String subDistrict;
  final LatLng latLng;

  CurrentPositionEntity({
    required this.addressName,
    required this.province,
    required this.district,
    required this.subDistrict,
    required this.latLng
  });


}