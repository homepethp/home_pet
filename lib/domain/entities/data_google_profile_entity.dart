class DataGoogleProfileEntity {
  final String id;
  final String idToken;
  final String name;
  final String firstName;
  final String lastName;
  final String email;
  final String imageURL;

  DataGoogleProfileEntity({
    required this.id,
    required this.idToken,
    required this.name,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.imageURL,
  });
}