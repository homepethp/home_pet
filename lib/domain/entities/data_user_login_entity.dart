class DataUserLoginEntity{
  final String username;
  final String password;

  DataUserLoginEntity({required this.username, required this.password});
}