class FileImagePetEntity{
  final String fileId;
  final String path;

  FileImagePetEntity({required this.fileId, required this.path});
}