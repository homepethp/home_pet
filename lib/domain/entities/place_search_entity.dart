class PlaceSearchEntity{
  final String addressName;
  final String placeId;

  PlaceSearchEntity({required this.addressName, required this.placeId});
}