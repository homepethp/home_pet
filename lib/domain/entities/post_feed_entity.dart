import 'package:home_pet/domain/entities/base_post_feed_entity.dart';
import 'package:home_pet/domain/entities/file_image_pet_entity.dart';

class PostFeedEntity extends BasePostFeedEntity{
  final String postId;
  final String userPostId;
  final String countLike;
  final String createAt;
  final String lastTime;
  final String date;
  final String agePet;
  final int statusPostId;
  final int sexPetId;
  final int typePetId;
  final String updateAt;
  final String updateBy;
  final UserPostEntity userPost;
  List<FileImagePetEntity> listImagePet = [];

  PostFeedEntity({
    required this.postId,
    required this.userPostId,
    required this.countLike,
    required this.createAt,
    required this.lastTime,
    required this.date,
    required this.agePet,
    required this.statusPostId,
    required this.sexPetId,
    required this.typePetId,
    required this.updateAt,
    required this.updateBy,
    required this.userPost,
    required this.listImagePet
  }) : super(statusPostId, sexPetId, typePetId);
}

///--------------------------------UserPostOnFeed----------------------------------------///

class UserPostEntity{
  final String avatar;
  final String nameAccount;

  UserPostEntity({required this.avatar, required this.nameAccount});
}
