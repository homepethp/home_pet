class UserAuthEntity{
  final String id;
  final String role;
  final String token;
  final String userName;

  UserAuthEntity({
    required this.id,
    required this.role,
    required this.token,
    required this.userName,
  });
}