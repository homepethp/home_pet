import 'package:home_pet/domain/entities/post_feed_entity.dart';

import '../../core/result/app_result.dart';
import '../../data/request/get_feed_request.dart';
import '../../data/response/base_response.dart';

abstract class HomeRepository{
  Future<AppResult<AppFeedResponse<List<PostFeedEntity>>>> getPostFeed(GetFeedRequest params);
}