import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/domain/entities/current_position_entity.dart';

import '../entities/place_search_entity.dart';

abstract class LocationRepository{

  Future<AppResult<CurrentPositionEntity>> getCurrentPosition();
  Future<AppResult<List<PlaceSearchEntity>>> searchLocationByKeyword(String keyword);

  Future<AppResult<CurrentPositionEntity>> patchPlaceSearch(PlaceSearchEntity params);
}