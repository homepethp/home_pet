import '../../core/result/app_result.dart';
import '../../data/request/post_request.dart';

abstract class PostRepository{

  Future<AppResult<void>> createPost(PostRequest params);
}