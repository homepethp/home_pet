import 'package:home_pet/core/result/app_event_result.dart';
import 'package:home_pet/data/request/email_request.dart';
import 'package:home_pet/data/request/login_request.dart';
import 'package:home_pet/data/request/register_request.dart';
import 'package:home_pet/data/request/verify_code_request.dart';
import 'package:home_pet/domain/entities/data_google_profile_entity.dart';
import 'package:home_pet/domain/entities/data_user_login_entity.dart';

import '../../core/result/app_result.dart';
import '../../data/request/login_with_google_request.dart';
import '../../data/request/save_user_login_request.dart';
import '../entities/user_auth_entity.dart';

abstract class UserAuthRepository{
  Future<void> saveSkipRecommend();
  Future<void> saveUserLogin(SaveUserLoginRequest params);

  Future<bool> getSkipRecommend();
  Future<String> getUserToken();
  Future<AppResult<DataUserLoginEntity>> getDataUserLogin();

  Future<AppResult<UserAuthEntity>> userLogin(LoginRequest params);
  Future<AppResult<UserAuthEntity>> userLoginWithGoogleToApi(LoginWithGoogleRequest params);
  Future<AppResult<DataGoogleProfileEntity>> userLoginWithGoogle();

  Future<AppResult<UserAuthEntity>> userRegister(RegisterRequest params);
  Future<AppResult<bool>> forgotPasswordByEmail(EmailRequest params);
  Future<AppResult<bool>> forgotPasswordByVerifyCode(VerifyCodeRequest params);

  Future<void> signOutFromGoogle();
}