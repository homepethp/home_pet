import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/repositories/location_repository.dart';

import '../../../core/result/app_result.dart';
import '../../entities/current_position_entity.dart';

class GetCurrentLocation implements UseCase<AppResult<CurrentPositionEntity>, NoParams>{
  final LocationRepository locationRepository;

  GetCurrentLocation({required this.locationRepository});

  @override
  Future<AppResult<CurrentPositionEntity>> call(NoParams params) {
    return locationRepository.getCurrentPosition();
  }
}