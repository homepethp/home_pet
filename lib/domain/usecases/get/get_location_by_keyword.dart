// ignore_for_file: avoid_renaming_method_parameters

import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/entities/place_search_entity.dart';
import 'package:home_pet/domain/repositories/location_repository.dart';

import '../../../core/result/app_result.dart';

class GetLocationByKeyword extends UseCase<AppResult<List<PlaceSearchEntity>>, String>{
  final LocationRepository locationRepository;

  GetLocationByKeyword({required this.locationRepository});

  @override
  Future<AppResult<List<PlaceSearchEntity>>> call(String keyword) {
    return locationRepository.searchLocationByKeyword(keyword);
  }
}