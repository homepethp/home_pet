import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/entities/data_google_profile_entity.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class GetLoginWithGoogle implements UseCase<AppResult<DataGoogleProfileEntity>, NoParams>{
  final UserAuthRepository userAuthRepository;

  GetLoginWithGoogle({required this.userAuthRepository});

  @override
  Future<AppResult<DataGoogleProfileEntity>> call(NoParams params) {
    return userAuthRepository.userLoginWithGoogle();
  }
}