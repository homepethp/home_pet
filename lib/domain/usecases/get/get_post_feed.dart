import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/get_feed_request.dart';
import 'package:home_pet/domain/repositories/home_repository.dart';

import '../../../core/result/app_result.dart';
import '../../../data/response/base_response.dart';
import '../../entities/post_feed_entity.dart';

class GetPostFeed implements UseCase<AppResult<AppFeedResponse<List<PostFeedEntity>>>, GetFeedRequest>{
  final HomeRepository homeRepository;

  GetPostFeed({required this.homeRepository});

  @override
  Future<AppResult<AppFeedResponse<List<PostFeedEntity>>>> call(GetFeedRequest params) {
    return homeRepository.getPostFeed(params);
  }

}