import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class GetUserToken implements UseCase<String, NoParams>{
  final UserAuthRepository userAuthRepository;

  GetUserToken({required this.userAuthRepository});

  @override
  Future<String> call(NoParams params) {
    return userAuthRepository.getUserToken();
  }
}