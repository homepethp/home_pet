import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/entities/data_user_login_entity.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class GetDataUserLogin implements UseCase<AppResult<DataUserLoginEntity>, NoParams>{
  final UserAuthRepository userAuthRepository;

  GetDataUserLogin({required this.userAuthRepository});

  @override
  Future<AppResult<DataUserLoginEntity>> call(NoParams params) {
    return userAuthRepository.getDataUserLogin();
  }

}