import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class GetSkipRecommend implements UseCase<bool, NoParams>{
  final UserAuthRepository userAuthRepository;

  GetSkipRecommend({required this.userAuthRepository});

  @override
  Future<bool> call(NoParams params) {
    return userAuthRepository.getSkipRecommend();
  }
}