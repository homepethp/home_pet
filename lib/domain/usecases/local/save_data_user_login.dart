import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/save_user_login_request.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class SaveDataUserLogin implements UseCase<void, SaveUserLoginRequest>{
  final UserAuthRepository userAuthRepository;

  SaveDataUserLogin({required this.userAuthRepository});

  @override
  Future<void> call(SaveUserLoginRequest params) {
    return userAuthRepository.saveUserLogin(params);
  }
}