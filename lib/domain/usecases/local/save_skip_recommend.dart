import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class SaveSkipRecommend implements UseCase<void, NoParams>{
  final UserAuthRepository userAuthRepository;

  SaveSkipRecommend({required this.userAuthRepository});

  @override
  Future<void> call(NoParams params) {
    return userAuthRepository.saveSkipRecommend();
  }

}