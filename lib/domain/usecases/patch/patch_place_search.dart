import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/repositories/location_repository.dart';

import '../../../core/result/app_result.dart';
import '../../entities/current_position_entity.dart';
import '../../entities/place_search_entity.dart';

class PatchPlaceSearch extends UseCase<AppResult<CurrentPositionEntity>, PlaceSearchEntity>{
  final LocationRepository locationRepository;

  PatchPlaceSearch({required this.locationRepository});

  @override
  Future<AppResult<CurrentPositionEntity>> call(PlaceSearchEntity params) {
    return locationRepository.patchPlaceSearch(params);
  }

}