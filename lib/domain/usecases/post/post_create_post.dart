import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/post_request.dart';
import 'package:home_pet/domain/repositories/post_repository.dart';

class PostCreatePost implements UseCase<AppResult<void>, PostRequest>{
  final PostRepository postRepository;

  PostCreatePost({required this.postRepository});

  @override
  Future<AppResult<void>> call(PostRequest params) {
    return postRepository.createPost(params);
  }
}