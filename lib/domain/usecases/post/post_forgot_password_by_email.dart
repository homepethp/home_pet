import 'package:home_pet/core/result/app_event_result.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/email_request.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class PostForgotPasswordByEmail implements UseCase<AppResult<bool>, EmailRequest>{
  final UserAuthRepository userAuthRepository;

  PostForgotPasswordByEmail({required this.userAuthRepository});

  @override
  Future<AppResult<bool>> call(EmailRequest params) {
    return userAuthRepository.forgotPasswordByEmail(params);
  }
}