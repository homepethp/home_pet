import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/verify_code_request.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class PostForgotPasswordByVerifyCode implements UseCase<AppResult<bool>, VerifyCodeRequest> {
  final UserAuthRepository userAuthRepository;

  PostForgotPasswordByVerifyCode({required this.userAuthRepository});

  @override
  Future<AppResult<bool>> call(VerifyCodeRequest params) {
    return userAuthRepository.forgotPasswordByVerifyCode(params);
  }
}
