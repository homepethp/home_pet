import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class PostLogoutFromGoogle implements UseCase<void, NoParams>{
  final UserAuthRepository userAuthRepository;

  PostLogoutFromGoogle({required this.userAuthRepository});

  @override
  Future<void> call(NoParams params) {
    return userAuthRepository.signOutFromGoogle();
  }
}