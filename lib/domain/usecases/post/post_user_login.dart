import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/login_request.dart';
import 'package:home_pet/domain/entities/user_auth_entity.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class PostUserLogin implements UseCase<AppResult<UserAuthEntity>, LoginRequest>{
  final UserAuthRepository userAuthRepository;

  PostUserLogin({required this.userAuthRepository});

  @override
  Future<AppResult<UserAuthEntity>> call(LoginRequest params) {
    return userAuthRepository.userLogin(params);
  }
}