import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/login_with_google_request.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

import '../../entities/user_auth_entity.dart';

class PostUserLoginWithGoogle implements UseCase<AppResult<UserAuthEntity>, LoginWithGoogleRequest>{
  final UserAuthRepository userAuthRepository;

  PostUserLoginWithGoogle({required this.userAuthRepository});

  @override
  Future<AppResult<UserAuthEntity>> call(LoginWithGoogleRequest params) {
    return userAuthRepository.userLoginWithGoogleToApi(params);
  }
}