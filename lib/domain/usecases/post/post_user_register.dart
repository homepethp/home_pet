import 'dart:io';

import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/request/register_request.dart';
import 'package:home_pet/domain/entities/user_auth_entity.dart';
import 'package:home_pet/domain/repositories/user_auth_repository.dart';

class PostUserRegister implements UseCase<AppResult<UserAuthEntity>, RegisterRequest>{

  final UserAuthRepository userAuthRepository;
  PostUserRegister({required this.userAuthRepository});

  @override
  Future<AppResult<UserAuthEntity>> call(RegisterRequest params) {
   return userAuthRepository.userRegister(params);
  }

}