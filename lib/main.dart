import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/font_config.dart';
import 'package:home_pet/config/languages_config.dart';

import 'config/routes/app_pages.dart';
import 'di/injector.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

    Injector.instance.dependencies();

    return ScreenUtilInit(
      designSize: (
          const Size(828, 1792)
      ),
      builder: (context, child){
        return GetMaterialApp(
          translations: LanguagesConfig.instance,
          locale: Get.deviceLocale,
          fallbackLocale: const Locale('en','US'),
          theme: ThemeData(
            fontFamily: Fonts.kaNit,
          ),
          builder: (context, widget){
            return MediaQuery(
              //Setting fonts does not change with system fonts size
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: widget!,
            );
          },
          initialRoute: AppPages.initial,
          getPages: AppPages.routes,
        );
      },
    );
  }
}