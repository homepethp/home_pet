import 'dart:async';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'config/api_google_key_config.dart';
import 'config/flavor_config.dart';
import 'firebase_options.dart';
import 'main.dart';

void main() async {
  FlavorConfig(
      flavor: Flavor.beta,
      values: FlavorValues(
          baseURL: 'http://203.150.243.165:9000/api/v1/',
          apiGoogleKey: ApiGoogleKeyConfig.apiGoogleKeyBeta
      )
  );

  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isIOS) {
    await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform,);
  } else {
    await Firebase.initializeApp();
  }

  runZonedGuarded(()=> runApp(const MyApp()),
          (error, stackTrace) {
        debugPrint('runZonedGuarded: Caught error in my root zone.');
      });
}