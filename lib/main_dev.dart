import 'dart:async';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:home_pet/config/api_google_key_config.dart';

import 'config/flavor_config.dart';
import 'firebase_options.dart';
import 'main.dart';

void main() async {
  FlavorConfig(
      flavor: Flavor.development,
      values: FlavorValues(
        baseURL: 'http://203.150.243.165:9000/api/v1/',
        apiGoogleKey: ApiGoogleKeyConfig.apiGoogleKeyDev
      )
  );

  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isIOS) {
    await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  } else {
    await Firebase.initializeApp();
  }

  runZonedGuarded(()=> runApp(const MyApp()),
          (error, stackTrace) {
        debugPrint('runZonedGuarded: Caught error in my root zone.');
      });
}