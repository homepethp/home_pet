import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/routes/app_routes.dart';
import 'package:home_pet/core/result/app_event_result.dart';
import 'package:home_pet/data/request/email_request.dart';
import 'package:home_pet/data/request/verify_code_request.dart';
import 'package:home_pet/data/response/base_response.dart';
import 'package:home_pet/domain/usecases/post/post_forgot_password_by_email.dart';
import 'package:home_pet/domain/usecases/post/post_forgot_password_by_verify_code.dart';
import 'package:home_pet/presentation/widgets/reuse/dialog/dialog_button_one_widget.dart';
import 'package:home_pet/presentation/widgets/reuse/dialog/loading_dialog_widget.dart';

import '../../../../core/result/app_event_result.dart';
import '../../../../core/result/app_result.dart';
import '../../../widgets/reuse/input_reuse_widget.dart';

class ForgotPasswordController extends GetxController with InputWidgetController{

  final PostForgotPasswordByEmail _postForgotPasswordByEmail = Get.find<PostForgotPasswordByEmail>();
  final PostForgotPasswordByVerifyCode _postForgotPasswordByVerifyCode = Get.find<PostForgotPasswordByVerifyCode>();

  final FocusNode _emailFocusNode = FocusNode();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _pinController = TextEditingController();

  Rx<FocusNode> get emailFocusNode => _emailFocusNode.obs;
  Rx<TextEditingController> get emailController => _emailController.obs;
  Rx<TextEditingController> get pinController => _pinController.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void requestForgotPasswordByEmail() async {
    Get.dialog(LoadingDialogWidget());
    EmailRequest params = EmailRequest(email: _emailController.text);

    AppResult<bool> appResult = await _postForgotPasswordByEmail.call(params);
    appResult.whenWithResult((result) {
      Get.back();
      if(result.value) Get.toNamed(Routes.forgotPasswordEnterCode);
    }, (error) {
      Get.back();
      final err = error.errorObject as BaseResponseError;
      if(!err.isSuccess) Get.dialog(DialogButtonOneWidget(title: 'txt_forgot_password_by_email_failed_title'.tr, description: 'txt_forgot_password_by_email_failed_description'.tr,));
    });
  }

  void requestForgotPasswordByVerifyCode() async {
    Get.dialog(LoadingDialogWidget());
    VerifyCodeRequest params = VerifyCodeRequest(code: _pinController.text, email: _emailController.text);

    AppResult<bool> appResult = await _postForgotPasswordByVerifyCode.call(params);
    appResult.whenWithResult((result) {
      Get.back();
      if(result.value) Get.toNamed(Routes.forgotPasswordEnterNewPassword);
    }, (error) {
      Get.back();
      final err = error.errorObject as BaseResponseError;
      if(!err.isSuccess) Get.dialog(DialogButtonOneWidget(title: 'txt_forgot_password_by_email_failed_title'.tr, description: 'txt_forgot_password_by_email_failed_description'.tr,));
    });
  }
}