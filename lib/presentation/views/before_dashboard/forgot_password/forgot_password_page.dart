import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/app_colors.dart';
import 'package:home_pet/presentation/views/before_dashboard/forgot_password/forgot_password_controller.dart';

import '../../../../config/routes/app_routes.dart';

class ForgotPasswordPage extends GetView<ForgotPasswordController>{
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> FocusScope.of(context).unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
              onPressed: ()=> Get.back(),
              icon: const Icon(Icons.arrow_back_ios_rounded, color: Colors.white,)
          ),
        ),
        body: Container(
          padding: EdgeInsets.only(top: 200.h),
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: AppColors.primaryGradient()
          ),
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children:  [
                  const Image(image: AssetImage('assets/png/logo_home_pet.png')),
                  Container(
                    margin: EdgeInsets.only(top: 100.h),
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 80.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('txt_forgot_password_enter_email_title'.tr, style: TextStyle(fontSize: 44.sp, color: Colors.white, fontWeight: FontWeight.w400),),
                        Container(
                          alignment: Alignment.centerLeft,
                          width: double.infinity,
                          padding: EdgeInsets.all(32.w),
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(16)
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('txt_forgot_password_enter_email_description'.tr, style: TextStyle(fontSize: 26.sp, color: Colors.white, fontWeight: FontWeight.w300)),
                              Container(
                                margin: EdgeInsets.only(top: 20.h),
                                child: controller.inputReuseWidget(
                                  focusNode: controller.emailFocusNode.value,
                                  txtController: controller.emailController.value,
                                  label: 'txt_email'.tr,
                                ),
                              ),
                              Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(top: 40.h, bottom: 10.h),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16)
                                ),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.blue,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                  ),
                                  onPressed: () => controller.requestForgotPasswordByEmail(),
                                  child: Text('txt_send'.tr),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}