import 'package:get/get.dart';
import 'package:home_pet/presentation/views/before_dashboard/forgot_password/forgot_password_controller.dart';

class ForgotPasswordBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<ForgotPasswordController>(ForgotPasswordController());
  }

}