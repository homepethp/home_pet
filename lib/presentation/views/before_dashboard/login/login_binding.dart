import 'package:get/get.dart';
import 'package:home_pet/presentation/views/before_dashboard/login/login_controller.dart';

class LoginBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<LoginController>(LoginController());
  }
}