import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/routes/app_routes.dart';
import 'package:home_pet/core/result/app_event_result.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/arguments/register_data_argument.dart';
import 'package:home_pet/data/request/login_request.dart';
import 'package:home_pet/data/request/login_with_google_request.dart';
import 'package:home_pet/data/response/base_response.dart';
import 'package:home_pet/domain/entities/data_google_profile_entity.dart';
import 'package:home_pet/domain/entities/user_auth_entity.dart';
import 'package:home_pet/domain/usecases/get/get_login_with_google.dart';
import 'package:home_pet/domain/usecases/local/save_data_user_login.dart';
import 'package:home_pet/domain/usecases/post/post_logout_from_google.dart';
import 'package:home_pet/domain/usecases/post/post_user_login.dart';
import 'package:home_pet/domain/usecases/post/post_user_login_with_google.dart';
import 'package:home_pet/presentation/widgets/reuse/dialog/dialog_button_one_widget.dart';
import 'package:home_pet/presentation/widgets/reuse/dialog/loading_dialog_widget.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../../../../data/request/save_user_login_request.dart';
import '../../../widgets/reuse/input_reuse_widget.dart';

class LoginController extends GetxController with InputWidgetController{

  final GetLoginWithGoogle _getLoginWithGoogle = Get.find<GetLoginWithGoogle>();
  final PostUserLogin _postUserLogin = Get.find<PostUserLogin>();
  final PostUserLoginWithGoogle _postUserLoginWithGoogle = Get.find<PostUserLoginWithGoogle>();
  final PostLogoutFromGoogle _postLogoutFromGoogle = Get.find<PostLogoutFromGoogle>();
  final SaveDataUserLogin _saveDataUserLogin = Get.find<SaveDataUserLogin>();

  final FocusNode _usernameFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final RxBool _isObscurePassword = RxBool(true);

  Rx<FocusNode> get usernameFocusNode => _usernameFocusNode.obs;
  Rx<FocusNode> get passwordFocusNode => _passwordFocusNode.obs;
  Rx<TextEditingController> get userNameController => _userNameController.obs;
  Rx<TextEditingController> get passwordController => _passwordController.obs;
  RxBool get isObscurePassword => _isObscurePassword;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    _usernameFocusNode.dispose();
    _passwordFocusNode.dispose();
    _userNameController.dispose();
    _passwordController.dispose();
    super.onClose();
  }

  void unFocusKeyboard(){
    if(_usernameFocusNode.hasFocus) _usernameFocusNode.unfocus();

    if(_passwordFocusNode.hasFocus) _passwordFocusNode.unfocus();
  }

  void navigatorToForgotPasswordPage(){
    Get.toNamed(Routes.forgotPassword);
  }

  void requestLogin() async {
    Get.dialog(LoadingDialogWidget());

    LoginRequest params = LoginRequest(username: _userNameController.text, password: _passwordController.text);

    AppResult<UserAuthEntity> appResult = await _postUserLogin.call(params);

    appResult.whenWithResult((result) async {
      Get.back();
      final params = SaveUserLoginRequest(
          username: result.value.userName,
          password: _passwordController.text,
          token: result.value.token
      );

      await _saveDataUserLogin.call(params);
      Get.offNamedUntil(Routes.dashBoard, (route)=> false);
    }, (error) {
      Get.back();
      final err = error.errorObject as BaseResponseError;
      if(err.resultCode == AppResponseStatus.Unauthorized){
        Get.dialog(DialogButtonOneWidget(
          title: 'txt_login_failed_title'.tr,
          description: 'txt_login_failed_description'.tr,
        ));
      }
    });
  }

  void requestLoginWithGoogle() async {
    await _postLogoutFromGoogle.call(NoParams());
    AppResult<DataGoogleProfileEntity> appResult = await _getLoginWithGoogle.call(NoParams());

    appResult.whenWithResult((result) {
      _requestLoginWithGoogleToApi(result.value);
    }, (error) => null);
  }

  Future<void> _requestLoginWithGoogleToApi(DataGoogleProfileEntity data) async {
    Get.dialog(LoadingDialogWidget());
    LoginWithGoogleRequest params = LoginWithGoogleRequest(googleId: data.id);

    AppResult<UserAuthEntity> appResult = await  _postUserLoginWithGoogle.call(params);

    appResult.whenWithResult((result) {
      Get.back();
      Get.toNamed(Routes.dashBoard);
    }, (error) {
      Get.back();
      Get.toNamed(Routes.register, arguments: RegisterDataArgument(dataGoogleProfileEntity: data));
    });
  }

  test() async {
    AuthorizationCredentialAppleID result = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
    );

    print(result);
  }

  void switchObscurePassword(){
    isObscurePassword.value = !isObscurePassword.value;
  }
}