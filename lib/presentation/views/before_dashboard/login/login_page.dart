import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/app_colors.dart';
import 'package:home_pet/config/font_config.dart';
import 'package:home_pet/config/routes/app_routes.dart';
import 'package:home_pet/presentation/views/before_dashboard/login/login_controller.dart';

class LoginPage extends GetView<LoginController>{
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> controller.unFocusKeyboard(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Obx(() => Container(
          decoration: BoxDecoration(
            gradient: AppColors.primaryGradient()
          ),
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 50.w),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Image(image: AssetImage('assets/png/logo_home_pet.png')),
                  Container(
                    margin: EdgeInsets.only(top: 20.h),
                    alignment: Alignment.centerLeft,
                      child: Text('txt_login'.tr, style: TextStyle(color: Colors.white, fontSize: 52.sp, fontWeight: FontWeight.w400),)
                  ),
                  Container(
                    padding: EdgeInsets.all(20.w),
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(16)
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 40.h),
                          child: controller.inputReuseWidget(
                            focusNode: controller.usernameFocusNode.value,
                            txtController: controller.userNameController.value,
                            label: 'txt_username'.tr,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 30.h, bottom: 10.h),
                          child: controller.inputReuseWidget(
                            focusNode: controller.passwordFocusNode.value,
                            txtController: controller.passwordController.value,
                            isPassword: true,
                            isObscure: controller.isObscurePassword.value,
                            label: 'txt_password'.tr,
                            switchObscurePassword: controller.switchObscurePassword
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: GestureDetector(
                            onTap: ()=> controller.navigatorToForgotPasswordPage(),
                              child: Text('txt_forgot_password'.tr, style: TextStyle(color: const Color(0xff003366), fontSize: 28.sp, fontWeight: FontWeight.w300),)),
                        ),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 40.h, bottom: 10.h),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16)
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                            ),
                            onPressed: ()=> controller.requestLogin(),
                            child: Text('txt_login'.tr),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20.w),
                          child: Row(
                            children: [
                              const Expanded(
                                  child: Divider(
                                    color: Colors.white,
                                  )
                              ),

                              Text(" OR ", style: TextStyle(color: Colors.white, fontSize: 28.sp,),),

                              const Expanded(
                                  child: Divider(
                                    color: Colors.white,
                                  )
                              ),
                            ],
                          ),
                        ),
                        _btnSignInGoogle(),
                        _btnSignInApple(),
                        Container(
                            margin: EdgeInsets.only(top: 30.h),
                            child: RichText(
                              text: TextSpan(
                                  text: 'txt_new_register'.tr,
                                  style: TextStyle(color: Colors.white, fontSize: 28.sp, fontWeight: FontWeight.w300, fontFamily: Fonts.kaNit),
                                  children: [
                                    TextSpan(
                                      recognizer: TapGestureRecognizer()..onTap = ()=> Get.toNamed(Routes.register),
                                      text: ' ${'txt_register'.tr}',
                                      style: TextStyle(color: const Color(0xff003366), fontSize: 30.sp, fontWeight: FontWeight.w500, fontFamily: Fonts.kaNit),
                                    )
                                  ]
                              ),
                            )
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        )),
      ),
    );
  }

  Widget _btnSignInGoogle()=> Container(
    width: double.infinity,
    margin: EdgeInsets.only(top: 20.h),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16)
    ),
    child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          primary: Colors.grey[100],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
        onPressed: ()=> controller.requestLoginWithGoogle(),
        icon: SvgPicture.asset('assets/svg/ic_google_login.svg', width: 50.w, height: 50.w,),
        label: Text('txt_login_google'.tr, style: TextStyle(color: Colors.black, fontSize: 28.sp, fontWeight: FontWeight.w400),)
    ),
  );

  Widget _btnSignInApple()=> Platform.isIOS ? Container(
    width: double.infinity,
    margin: EdgeInsets.only(top: 10.h),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16)
    ),
    child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          primary: Colors.black,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
        onPressed: ()=> controller.test(),
        icon: SvgPicture.asset('assets/svg/ic_apple_login.svg', width: 50.w, height: 50.w,),
        label: Text('txt_login_apple'.tr, style: TextStyle(color: Colors.white, fontSize: 28.sp, fontWeight: FontWeight.w400),)
    ),
  ) : Container();
}