import 'package:get/get.dart';
import 'package:home_pet/presentation/views/before_dashboard/recommend/recommend_controller.dart';

class RecommendBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<RecommendController>(RecommendController());
  }
}