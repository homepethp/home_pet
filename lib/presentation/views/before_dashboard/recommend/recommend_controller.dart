import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/models/slide_recommend_model.dart';
import 'package:home_pet/domain/usecases/local/save_skip_recommend.dart';

import '../../../../config/routes/app_routes.dart';

class RecommendController extends GetxController{

  final SaveSkipRecommend _saveSkipRecommend = Get.find<SaveSkipRecommend>();

  final RxInt _currentPage = RxInt(0);
  final PageController _pageController = PageController(initialPage: 0);

  Rx<PageController> get pageController => _pageController.obs;
  RxInt get currentPage => _currentPage;
  RxList<SlideRecommendModel> get slidePageList => slideList.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
    _pageController.dispose();
  }

  void onPageChanged(int index, {bool isButton = false}) {
      _currentPage.value = index;

      if(isButton){
        _pageController.animateToPage(
          _currentPage.value,
          duration: const Duration(milliseconds: 100),
          curve: Curves.ease,
        );
      }
  }

  void backToFirstRecommend(){
    onPageChanged(currentPage.value -= 1, isButton: true);
  }

  void nextToRecommend() async {
    if(currentPage.value == 0) {
      onPageChanged(currentPage.value += 1, isButton: true);
    }else{
      await _saveSkipRecommend.call(NoParams());
      Get.offNamedUntil(Routes.login, (route) => false);
    }
  }
}