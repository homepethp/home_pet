import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/routes/app_routes.dart';
import 'package:home_pet/data/models/slide_recommend_model.dart';
import 'package:home_pet/presentation/views/before_dashboard/recommend/recommend_controller.dart';
import 'package:home_pet/presentation/widgets/before_dashboard/recommend/slide_recommmend_item_widget.dart';

import '../../../widgets/reuse/slide_dots.dart';

class RecommendPage extends GetView<RecommendController>{
  const RecommendPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // padding: EdgeInsets.symmetric(horizontal: 20.w),
        color: Colors.white,
        child: Column(
          children: [
             Expanded(
               child: Stack(
                 alignment: AlignmentDirectional.bottomCenter,
                 children: [
                   PageView.builder(
                     scrollDirection: Axis.horizontal,
                     controller: controller.pageController.value,
                     onPageChanged: controller.onPageChanged,
                     itemCount: controller.slidePageList.length,
                     itemBuilder: (ctx, i) => SlideRecommendItemWidget(index: i, listSlide: controller.slidePageList)
                   ),
                   Obx(() => Stack(
                     alignment: AlignmentDirectional.topStart,
                     children: <Widget>[
                       Container(
                         margin: EdgeInsets.only(bottom: 20.h),
                         alignment: Alignment.bottomCenter,
                         padding: EdgeInsets.symmetric(horizontal: 20.w),
                         child: Stack(
                           alignment: Alignment.center,
                           children: [
                             Row(
                               mainAxisSize: MainAxisSize.max,
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: _dotsSlideWidget(controller.slidePageList, controller.currentPage.value),
                             ),
                             Row(
                               crossAxisAlignment: CrossAxisAlignment.center,
                               mainAxisSize: MainAxisSize.max,
                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                               children: [
                                 controller.currentPage.value == 1
                                     ? ElevatedButton(
                                     style: ElevatedButton.styleFrom(
                                       padding: EdgeInsets.symmetric(horizontal: 50.w),
                                       shape: RoundedRectangleBorder(
                                         borderRadius: BorderRadius.circular(30.0),
                                       ),
                                     ),
                                     onPressed: ()=> controller.backToFirstRecommend(),
                                     child: Text('all_back'.tr)
                                 )
                                     : Container(height: 0,),
                                 ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      padding: EdgeInsets.symmetric(horizontal: 50.w),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30.0),
                                      ),
                                    ),
                                     onPressed: ()=> controller.nextToRecommend(),
                                     child: Text('all_next'.tr)
                                 )
                               ],
                             ),
                           ],
                         ),
                       )
                     ],
                   ))
                 ],
               ),
             ),
          ],
        ),
      ),
    );
  }

  List<Widget> _dotsSlideWidget(List<SlideRecommendModel> slidePageList, int currentPage){
    List<Widget> listWidget = [
      for(int i = 0; i < slidePageList.length; i++)
        if(i == currentPage)
          const SlideDots(true)
        else
          const SlideDots(false),
    ];

    return listWidget;
  }
}