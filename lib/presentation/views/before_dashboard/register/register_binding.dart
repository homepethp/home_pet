import 'package:get/get.dart';
import 'package:home_pet/presentation/views/before_dashboard/register/register_controller.dart';

class RegisterBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<RegisterController>(RegisterController());
  }
}