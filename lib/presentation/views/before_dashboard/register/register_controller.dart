import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/arguments/register_data_argument.dart';
import 'package:home_pet/data/request/register_request.dart';
import 'package:home_pet/data/request/save_user_login_request.dart';
import 'package:home_pet/data/response/base_response.dart';
import 'package:home_pet/domain/entities/data_google_profile_entity.dart';
import 'package:home_pet/domain/entities/user_auth_entity.dart';
import 'package:home_pet/domain/usecases/local/save_data_user_login.dart';
import 'package:home_pet/domain/usecases/post/post_user_register.dart';
import 'package:home_pet/presentation/widgets/reuse/dialog/loading_dialog_widget.dart';
import 'package:home_pet/utils/app_input_utils.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../config/routes/app_routes.dart';
import '../../../widgets/reuse/bottom_sheet_widget/select_image_bottom_sheet.dart';
import '../../../widgets/reuse/input_reuse_widget.dart';

class RegisterController extends GetxController with InputWidgetController{

  final PostUserRegister _postUserRegister = Get.find<PostUserRegister>();
  final SaveDataUserLogin _saveDataUserLogin = Get.find<SaveDataUserLogin>();
  final AppInputUtils _appInputUtils = Get.find<AppInputUtils>();

  final FocusNode _nameAccountFocusNode = FocusNode();
  final FocusNode _usernameFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _telFocusNode = FocusNode();
  final TextEditingController _nameAccountController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _telController = TextEditingController();
  final Rx<String> _pathFileImage = Rx('');
  final Rx<String?> _txtNameAccountInvalid = Rx(null);
  final Rx<String?> _txtUsernameInvalid = Rx(null);
  final Rx<String?> _txtEmailInvalid = Rx(null);
  final Rx<String?> _txtPasswordInvalid = Rx(null);
  final Rx<String?> _txtTelInvalid = Rx(null);
  final RxBool _isObscurePassword = RxBool(true);
  RegisterDataArgument? _dataGoogleProfileEntity;

  Rx<FocusNode> get nameAccountFocusNode => _nameAccountFocusNode.obs;
  Rx<FocusNode> get usernameFocusNode => _usernameFocusNode.obs;
  Rx<FocusNode> get passwordFocusNode => _passwordFocusNode.obs;
  Rx<FocusNode> get emailFocusNode => _emailFocusNode.obs;
  Rx<FocusNode> get telFocusNode => _telFocusNode.obs;
  Rx<TextEditingController> get nameAccountController => _nameAccountController.obs;
  Rx<TextEditingController> get userNameController => _userNameController.obs;
  Rx<TextEditingController> get passwordController => _passwordController.obs;
  Rx<TextEditingController> get emailController => _emailController.obs;
  Rx<TextEditingController> get telController => _telController.obs;
  Rx<String> get pathFileImage => _pathFileImage;
  Rx<String?> get txtNameAccountInvalid => _txtNameAccountInvalid;
  Rx<String?> get txtUsernameInvalid => _txtUsernameInvalid;
  Rx<String?> get txtEmailValid => _txtEmailInvalid;
  Rx<String?> get txtPasswordInvalid => _txtPasswordInvalid;
  Rx<String?> get txtTelInvalid => _txtTelInvalid;
  RxBool get isObscurePassword => _isObscurePassword;

  @override
  void onInit() {
    if(Get.arguments != null) _dataGoogleProfileEntity = Get.arguments as RegisterDataArgument;
    _userNameController.addListener(() {
      _txtUsernameInvalid.value = null;
    });
    _emailInvalid();
    _passwordInvalid();
    _telInvalid();
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  // void _nameAccountInvalid(){
  //   _nameAccountController.addListener(() {
  //     _txtNameAccountInvalid.value = null;
  //     if(_nameAccountController.text.isNotEmpty){
  //       !_appInputUtils.checkEmailValidator(_nameAccountController.text)
  //           ? _txtNameAccountInvalid.value = 'txt_name_account_invalid'.tr
  //           : _txtNameAccountInvalid.value = null;
  //     } else{
  //       txtEmailValid.value = null;
  //     }
  //   });
  // }

  void _emailInvalid(){
    _emailController.addListener(() {
      if(_emailController.text.isNotEmpty){
        !_appInputUtils.checkEmailValidator(_emailController.text)
            ? _txtEmailInvalid.value = 'txt_email_invalid'.tr
            : _txtEmailInvalid.value = null;
      } else{
        txtEmailValid.value = null;
      }
    });
  }

  void _passwordInvalid(){
    _passwordController.addListener(() {
      if(_passwordController.text.isNotEmpty){
        !_appInputUtils.checkPasswordValidator(_passwordController.text)
            ? _txtPasswordInvalid.value = 'txt_password_invalid'.tr
            : _txtPasswordInvalid.value = null;
      } else{
        _txtPasswordInvalid.value = null;
      }
    });
  }

  void _telInvalid(){
    _telController.addListener(() {
      if(_telController.text.isNotEmpty){
        !_appInputUtils.checkTelValidator(_telController.text)
            ? _txtTelInvalid.value = 'txt_tel_invalid'.tr
            : _txtTelInvalid.value = null;
      } else{
        _txtTelInvalid.value = null;
      }
    });
  }

  void unFocusKeyboard(){
    if(_usernameFocusNode.hasFocus) _usernameFocusNode.unfocus();
    if(_passwordFocusNode.hasFocus) _passwordFocusNode.unfocus();
    if(_emailFocusNode.hasFocus) _emailFocusNode.unfocus();
    if(_telFocusNode.hasFocus) _telFocusNode.unfocus();
  }

  void showActionSheetImage(){
    Get.bottomSheet(
      SelectImageBottomSheet(
          firstCallback: ()=> {Get.back(), getImageFromCamera()},
          secondCallback: ()=> {Get.back(), getImageFromGallery()}),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.r),
      ),
    );
  }

  void getImageFromCamera() async {
    await ImagePicker().pickImage(source: ImageSource.camera, imageQuality: 50).then((value) async {
      _pathFileImage.value = value!.path;
    });
  }

  void getImageFromGallery() async {
    await ImagePicker().pickImage(source: ImageSource.gallery, imageQuality: 50).then((value) async {
      _pathFileImage.value = value!.path;
    });
  }

  void requestRegister() async {
    if(_nameAccountController.text.isEmpty || _userNameController.text.isEmpty || _passwordController.text.isEmpty || _emailController.text.isEmpty || _telController.text.isEmpty){
      Get.snackbar('', 'txt_register_input_is_empty_failed'.tr, snackPosition: SnackPosition.BOTTOM, backgroundColor: Colors.white, titleText: Text('txt_register_failed'.tr, style: TextStyle(color: Colors.red, fontSize: 34.sp),));
    }else if(_pathFileImage.value.isEmpty){
      Get.snackbar('', 'txt_register_image_is_empty_failed'.tr, snackPosition: SnackPosition.BOTTOM, backgroundColor: Colors.white, titleText: Text('txt_register_failed'.tr, style: TextStyle(color: Colors.red, fontSize: 34.sp),));
    }else{
      Get.dialog(LoadingDialogWidget());

      final params = RegisterRequest(
          fileImage: File(_pathFileImage.value),
          nameAccount: _nameAccountController.text,
          username: _userNameController.text,
          password: _passwordController.text,
          email: _emailController.text,
          phone: _telController.text,
          appleId: null,
          googleAvatar: _dataGoogleProfileEntity?.dataGoogleProfileEntity?.imageURL,
          googleId: _dataGoogleProfileEntity?.dataGoogleProfileEntity?.id,
          googleIdToken: _dataGoogleProfileEntity?.dataGoogleProfileEntity?.idToken
      );

      AppResult<UserAuthEntity> appResult = await _postUserRegister.call(params);

      appResult.whenWithResult((result) async {
        Get.back();
        final params = SaveUserLoginRequest(
            username: result.value.userName,
            password: _passwordController.text,
            token: result.value.token
        );

        await _saveDataUserLogin.call(params);

        Get.offNamedUntil(Routes.dashBoard, (route) => false);
      }, (error) {
        Get.back();
        Get.snackbar('', 'txt_register_is_information_duplicate'.tr, snackPosition: SnackPosition.BOTTOM, backgroundColor: Colors.white, titleText: Text('txt_register_failed'.tr, style: TextStyle(color: Colors.red, fontSize: 34.sp),));
        final err = error.errorObject as BaseResponseError;
        print(err);
        final data = err.dataError as List<dynamic>;
        for (Map<String, dynamic> element in data) {
          element.forEach((key, value) {
            switch(value){
              case 'txt_username_duplicate': _txtUsernameInvalid.value = 'txt_username_duplicate'.tr;
              break;
              case 'txt_email_duplicate': _txtEmailInvalid.value = 'txt_email_duplicate'.tr;
              break;
              case 'txt_phone_duplicate': _txtTelInvalid.value = 'txt_phone_duplicate'.tr;
              break;
              default: '';
            }
          });
        }
      });
    }
  }

  void switchObscurePassword(){
    isObscurePassword.value = !isObscurePassword.value;
  }
}