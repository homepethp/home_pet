import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/app_colors.dart';
import 'package:home_pet/presentation/views/before_dashboard/register/register_controller.dart';

class RegisterPage extends GetView<RegisterController>{
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> controller.unFocusKeyboard(),
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
              onPressed: ()=> Get.back(),
              icon: const Icon(Icons.arrow_back_ios_rounded, color: Colors.white)
          ),
        ),
        body: Obx(() => Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              gradient: AppColors.primaryGradient()
          ),
          height: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 40.w),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                _imageUser(context, controller.pathFileImage.value),
                Container(
                    margin: EdgeInsets.only(top: 20.h),
                    alignment: Alignment.centerLeft,
                    child: Text('txt_register'.tr, style: TextStyle(color: Colors.white, fontSize: 52.sp, fontWeight: FontWeight.w400),)
                ),
                Container(
                  margin: EdgeInsets.only(top: 40.h),
                  child: controller.inputReuseWidget(
                    focusNode: controller.nameAccountFocusNode.value,
                    txtController: controller.nameAccountController.value,
                    txtError: controller.txtNameAccountInvalid.value,
                    label: 'txt_name_account'.tr,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40.h),
                  child: controller.inputReuseWidget(
                    focusNode: controller.usernameFocusNode.value,
                    txtController: controller.userNameController.value,
                    txtError: controller.txtUsernameInvalid.value,
                    label: 'txt_username'.tr,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 30.h),
                  child: controller.inputReuseWidget(
                    focusNode: controller.passwordFocusNode.value,
                    txtController: controller.passwordController.value,
                    txtError: controller.txtPasswordInvalid.value,
                    maxLength: 8,
                    isPassword: true,
                    isObscure: controller.isObscurePassword.value,
                    label: 'txt_password'.tr,
                    switchObscurePassword: controller.switchObscurePassword
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 30.h),
                  child: controller.inputReuseWidget(
                    focusNode: controller.emailFocusNode.value,
                    txtController: controller.emailController.value,
                    txtError: controller.txtEmailValid.value,
                    label: 'txt_email'.tr,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 30.h),
                  child: controller.inputReuseWidget(
                    focusNode: controller.telFocusNode.value,
                    txtController: controller.telController.value,
                    textInputType: TextInputType.phone,
                    txtError: controller.txtTelInvalid.value,
                    maxLength: 10,
                    label: 'txt_tel'.tr,
                  ),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 40.h, bottom: 10.h),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                    onPressed: ()=> controller.requestRegister(),
                    child: Text('txt_register'.tr),
                  ),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }

  Widget _imageUser(context, String fileImage)=> SizedBox(
    width: 300.w,
    height: 300.w,
    child: Stack(
      clipBehavior: Clip.none,
      fit: StackFit.expand,
      children: [
        Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
              gradient: const LinearGradient(
                  colors: [
                    Color(0xFF66ccff),
                    Color(0xFF0066cc),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter
              ),
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 4,
                  blurRadius: 10,
                  offset: const Offset(0, 3),
                )
              ]
          ),
          child: fileImage != ''
              ? ClipOval(child: Image.file(File(fileImage), fit: BoxFit.cover, width: 270.w, height: 270.w,))
              : ClipOval(child: SvgPicture.asset('assets/svg/ic_default_people.svg', fit: BoxFit.cover, width: 270.w, height: 270.w, color: Colors.white,))

        ),
        Positioned(
            right: 10,
            bottom: 5,
            child: Container(
                alignment: Alignment.center,
                height: 60.w,
                width: 60.w,
                decoration:  BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      spreadRadius: 4,
                      blurRadius: 5,
                      offset: const Offset(0, 3),
                    )
                  ]
                ),
                child: RawMaterialButton(
                  onPressed: ()=> controller.showActionSheetImage(),
                  elevation: 2.0,
                  fillColor: Colors.white,
                  shape: const CircleBorder(),
                  child: Icon(Icons.camera_alt_outlined, color: Colors.blue.shade900, size: 20),
                )))
      ],
    ),
  );

}