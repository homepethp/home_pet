import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:home_pet/presentation/views/chat/chat_controller.dart';

class ChatPage extends GetView<ChatController>{
  const ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Text('ChatPage'),
      ),
    );
  }
}