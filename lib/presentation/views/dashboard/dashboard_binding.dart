import 'package:get/get.dart';
import 'package:home_pet/presentation/views/dashboard/dashboard_controller.dart';
import 'package:home_pet/presentation/views/home/home_controller.dart';


class DashBoardBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<DashBoardController>(DashBoardController());
    Get.put<HomeController>(HomeController(), permanent: true);
  }
}