import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:home_pet/presentation/views/chat/chat_page.dart';
import 'package:home_pet/presentation/views/home/home_page.dart';
import 'package:home_pet/presentation/views/setting/setting_page.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';

class DashBoardController extends GetxController{

  final RxList<Widget> _buildScreen = RxList([]);
  final RxList<PersistentBottomNavBarItem> _navBarsItems = RxList([]);
  final PersistentTabController _persistentTabController = PersistentTabController();

  RxList<Widget> get buildScreen => _buildScreen;
  RxList<PersistentBottomNavBarItem> get navBarsItems => _navBarsItems;
  Rx<PersistentTabController> get persistentTabController => _persistentTabController.obs;

  @override
  void onInit() {
    _persistentTabController.obs.value = PersistentTabController(initialIndex: 0);
    _initialBuildListScreen();
    _initialNavBarsItems();
    super.onInit();
  }

  @override
  void onClose() {

    super.onClose();
  }

  void _initialBuildListScreen(){
    _buildScreen.obs.value.addAll([
      HomePage(),
      const ChatPage(),
      const SettingPage()
    ]);
  }

  void _initialNavBarsItems(){
    _navBarsItems.value = [
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.home),
        title: ("Home"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(CupertinoIcons.chat_bubble_2_fill),
        title: ("Chat"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.settings_rounded),
        title: ("Settings"),
        activeColorPrimary: CupertinoColors.activeBlue,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }
}