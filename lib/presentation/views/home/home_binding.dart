import 'package:get/get.dart';
import 'package:home_pet/presentation/views/home/home_controller.dart';

class HomeBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<HomeController>(Get.find<HomeController>());
  }
}