import 'package:get/get.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/request/get_feed_request.dart';
import 'package:home_pet/data/response/base_response.dart';
import 'package:home_pet/domain/entities/post_feed_entity.dart';
import 'package:home_pet/domain/usecases/get/get_post_feed.dart';
import 'package:home_pet/presentation/widgets/reuse/input_reuse_widget.dart';

class HomeController extends GetxController with InputWidgetController {

  final GetPostFeed _getPostFeed = Get.find<GetPostFeed>();


  @override
  void onReady() {
    getFeed();
    super.onReady();
  }

  @override
  void onInit() {
    // getFeed();
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> getFeed({int numPage = 0, String typePet = ''}) async {
    GetFeedRequest params = GetFeedRequest(numPage: numPage, typePet: typePet);

    AppResult<AppFeedResponse<List<PostFeedEntity>>> appResult = await _getPostFeed.call(params);

    appResult.whenWithResult((result) {
      print(result.value);
    }, (error) {

    });
  }
}