import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:home_pet/presentation/views/home/home_binding.dart';
import 'package:home_pet/presentation/views/home/home_controller.dart';
import 'package:home_pet/presentation/widgets/reuse/matiral/reuse_sliver_appbar_widget.dart';

import '../../../config/app_colors.dart';
import '../../../config/routes/app_routes.dart';

class HomePage extends GetView<HomeController>{
  HomePage({Key? key}) : super(key: key);

  final List<dynamic> _listFilter = [
    {'icon' : 'icon_pets/ic_dog.png', 'title' : 'Dog'},
    {'icon' : 'icon_pets/ic_cat.png', 'title' : 'Cat'},
    {'icon' : 'icon_pets/ic_bird.png', 'title' : 'Bird'},
    {'icon' : 'ic_other.png', 'title' : 'Other'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        physics: const BouncingScrollPhysics(),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            ReuseSilverAppbarWidget(
              title: 'Home Pet',
              innerBoxIsScrolled: innerBoxIsScrolled,
              actions: [
                const Icon(Icons.favorite),
                SizedBox(width: 30.w),
                const Icon(Icons.notifications),
                SizedBox(width: 30.w)
              ],
              leading: SizedBox(width: 30.w,),
            )
          ];
        },
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 40.w, vertical: 20.h),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      const Image(image: AssetImage('assets/png/ic_filter.png')),
                      SizedBox(width: 10.w,),
                      Text('Filter', style: TextStyle(fontSize: 40.sp, color: const Color(0xff026DB5), fontWeight: FontWeight.w300),),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10.h),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      physics: const BouncingScrollPhysics(),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: _listFilterWidget(),
                      ),
                    ),
                  ),
                  Divider(
                    height: 5.h,
                    color: const Color(0xff33CCFF),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20.h),
                    child: Column(
                      children: _feedWidget(),
                    ),
                  )
                ],
              ),
            )
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ()=> Get.toNamed(Routes.post),
        elevation: 0.0,
        child: Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: AppColors.primaryGradient(),
              boxShadow: [
                BoxShadow(
                  color: Colors.purple.withOpacity(0.3),
                  blurRadius: 7,
                  offset: const Offset(0, 0.2),
                ),
              ],
            ),
            child: const Icon(Icons.add)
        ),
      ),
    );
  }

  List<Widget> _listFilterWidget(){
    List<Widget> widgetList = [];

    for (Map<String, dynamic> item in _listFilter) {
      widgetList.add(
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(right: 20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image(image: AssetImage('assets/png/${item['icon']}'), width: 110.w, height: 110.w,),
                Text(item['title'], style: TextStyle(color: const Color(0xff026DB5), fontSize: 32.sp, fontWeight: FontWeight.w400),)
              ],
            ),
          )
      );
    }

    return widgetList;
  }

  List<Widget> _feedWidget(){
    List<Widget> widgetList = [];

    for(int i = 0; i < 5; i++){
      widgetList.add(
          Container(
            padding: EdgeInsets.only(top: 20.h, bottom: 40.h),
            decoration: const BoxDecoration(
              border: Border(bottom: BorderSide(width: 0.5, color: Color(0xff33CCFF)))
            ),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.blue
                        ),
                        child: ClipOval(child: SvgPicture.asset('assets/svg/ic_default_people.svg', fit: BoxFit.cover, width: 100.w, height: 100.w))
                    ),
                    SizedBox(width: 20.w,),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('เสี่ยโอ เห็นกูเป็นเสี่ยเลียกูกันใหญ่', style: TextStyle(color: Color(0xff003366), fontSize: 32.sp, fontWeight: FontWeight.w500, overflow: TextOverflow.ellipsis),),
                                Text('เมื่อ 1 ชั่วโมงที่แล้ว', style: TextStyle(color: Colors.black, fontSize: 26.sp, fontWeight: FontWeight.w300),),
                              ],
                            ),
                          ),
                          Icon(Icons.favorite_border, color: Colors.red, size: 60.w,)
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 20.h),
                  child: Text('คิดถึงแมวบนฟ้า', style: TextStyle(color: Colors.black, fontSize: 30.sp, fontWeight: FontWeight.w400, overflow: TextOverflow.ellipsis), maxLines: 1,),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 660.h,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Image.network('https://ichef.bbci.co.uk/news/640/cpsprodpb/1124F/production/_119932207_indifferentcatgettyimages.png', fit: BoxFit.cover)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.h),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Image(image: AssetImage('assets/png/ic_like.png'), color: Colors.blue,),
                            SizedBox(width: 35.w),
                            const Image(image: AssetImage('assets/png/ic_chat.png'), color: Colors.blue,),
                            SizedBox(width: 30.w),
                            const Image(image: AssetImage('assets/png/ic_share.png'), color: Colors.blue,),
                          ],
                        ),
                      ),
                      Container(
                        height: 90.h,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                        ),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(horizontal: 50.w),
                            primary: Colors.blue,
                            // shadowColor: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16),
                            ),
                          ),
                          onPressed: (){},
                          child: Text('ดูเพิ่มเติม', style: TextStyle(color: Colors.white, fontSize: 32.sp, fontWeight: FontWeight.w300),),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
      );
    }

    return widgetList;
  }
}