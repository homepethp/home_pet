import 'package:get/get.dart';
import 'package:home_pet/presentation/views/post/post_controller.dart';

class PostBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<PostController>(PostController());
  }
}