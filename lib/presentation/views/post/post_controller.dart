import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/data/request/post_request.dart';
import 'package:home_pet/domain/entities/base_pets_entity.dart';
import 'package:home_pet/domain/entities/base_topic_entity.dart';
import 'package:home_pet/domain/usecases/post/post_create_post.dart';
import 'package:home_pet/presentation/widgets/reuse/dialog/loading_dialog_widget.dart';
import 'package:home_pet/presentation/widgets/reuse/input_reuse_widget.dart';
import 'package:image_picker/image_picker.dart';

import '../../../domain/entities/current_position_entity.dart';
import '../../widgets/reuse/bottom_sheet_widget/select_image_bottom_sheet.dart';

class PostController extends GetxController with InputWidgetController{

  final PostCreatePost _postCreatePost = Get.find<PostCreatePost>();
  final BasePetsEntity _basePetsEntity = Get.find<BasePetsEntity>();

  final RxList<String> _listPicture = RxList([]);
  final TextEditingController _descriptionInputController = TextEditingController();
  CurrentPositionEntity? _currentPositionEntityObs;
  final Rx<PetSexDataEntity> _sexPet = Get.find<BasePetsEntity>().listSexPet.first.obs;
  final RxString _agePet = Get.find<BasePetsEntity>().listAgePet.first.obs;
  final Rx<PetTypeDataEntity> _typePet = Get.find<BasePetsEntity>().listPetType.first.obs;

  BasePetsEntity get basePetsEntity => _basePetsEntity;
  Rx<TextEditingController> get descriptionInputController => _descriptionInputController.obs;
  RxList<String> get listPicture => _listPicture;
  Rx<CurrentPositionEntity?> get currentPositionEntityObs => _currentPositionEntityObs.obs;
  Rx<PetSexDataEntity> get sexPet => _sexPet;
  RxString get agePet => _agePet;
  Rx<PetTypeDataEntity> get typePet => _typePet;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void showActionSheetImage(){
    if(_listPicture.length < 5){
      Get.bottomSheet(
        SelectImageBottomSheet(
            firstCallback: ()=> {Get.back(), getImageFromCamera()},
            secondCallback: ()=> {Get.back(), getImageFromGallery()}),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.r),
        ),
      );
    }
  }

  void getImageFromCamera() async {
    await ImagePicker().pickImage(source: ImageSource.camera, imageQuality: 50, maxHeight: 1000, maxWidth: 1000).then((value) async {
      _listPicture.add(value!.path);
    });
  }

  void getImageFromGallery() async {
    await ImagePicker().pickImage(source: ImageSource.gallery, imageQuality: 50, maxWidth: 1000, maxHeight: 1000).then((value) async {
      _listPicture.add(value!.path);
    });
  }

  void removePicture(String value){
    int index = _listPicture.indexWhere((element) => element == value);
    _listPicture.removeAt(index);
  }

  void setSexPet(PetSexDataEntity data){
    _sexPet.value = data;
  }

  void setAgePet(String data){
    _agePet.value = data;
  }

  void setTypePet(PetTypeDataEntity data){
    _typePet.value = data;
  }

  void setLocation(CurrentPositionEntity currentPositionEntity){
    _currentPositionEntityObs = currentPositionEntity;
    update();
  }

  Future<void> requestCreatePost() async {

    Get.dialog(LoadingDialogWidget());

    List<File> listPicture = [];

    for(String item in _listPicture){
      listPicture.add(File(item));
    }

    PostRequest params = PostRequest(
      listPicture: listPicture,
      description: _descriptionInputController.text,
      sex: _sexPet.value.sexId,
      age: _agePet.value,
      typePet: _typePet.value.typeId,
      location: _currentPositionEntityObs!
    );

    AppResult<void> appResult = await _postCreatePost.call(params);

    appResult.whenWithResult((result) {
      Get.back();
      Get.back();
    }, (error) {
      Get.back();
    });

  }
}