import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/domain/entities/current_position_entity.dart';
import 'package:home_pet/presentation/views/post/post_controller.dart';
import 'package:home_pet/presentation/widgets/post/bottom_sheet_widget/select_age_bottomsheet_widget.dart';
import 'package:home_pet/presentation/widgets/post/bottom_sheet_widget/select_sex_bottomsheet_widget.dart';
import 'package:home_pet/presentation/widgets/post/bottom_sheet_widget/select_type_pet_bottomsheet_widget.dart';
import 'package:home_pet/presentation/widgets/reuse/matiral/reuser_appbar_widget.dart';

import '../../../config/routes/app_routes.dart';

class PostPage extends GetView<PostController>{
  const PostPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => GestureDetector(
      onTap: ()=> FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: ReuseAppbarWidget(
          title: 'txt_post'.tr,
        ),
        body: Container(
          padding: EdgeInsets.only(top: 50.h, left: 30.w, right: 30.w),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  width: double.infinity,
                  child: Wrap(
                      alignment: WrapAlignment.start,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      spacing: -3,
                      runSpacing: 0,
                      children: _listPictureWidget()
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 30.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text('${controller.listPicture.length}/5'),
                      const Divider(
                        thickness: 0.5,
                        height: 1,
                        color: Colors.blue,
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('txt_description'.tr, style: TextStyle(fontSize: 32.sp, color: Colors.black, fontWeight: FontWeight.w400),),
                    controller.inputReuseWidget(
                        txtController: controller.descriptionInputController.value,
                        textInputType: TextInputType.multiline,
                        maxLines: 6,
                        txtColor: Colors.black,
                        cursorColor: Colors.black,
                        hintText: 'Write a description'
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 50.h),
                  child: Column(
                    children: [
                      GestureDetector(
                        onTap: ()=> Get.bottomSheet(SelectSexBottomSheetWidget(postController: controller,)),
                        child: Container(
                          margin: EdgeInsets.only(top: 30.h),
                          padding: EdgeInsets.symmetric(vertical: 20.h),
                          width: double.infinity,
                          decoration: const BoxDecoration(
                              border: Border(bottom: BorderSide(color: Colors.blue, width: 1))
                          ),
                          child: Row(
                            children: [
                              const Icon(Icons.access_time_filled, color: Colors.blue,),
                              Expanded(child: Container(margin: EdgeInsets.only(left: 10.w), child: Text('sex', style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300),))),
                              Text(controller.sexPet.value.nameSex, style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300)),
                              Icon(Icons.arrow_forward_ios, color: Color(0xff727678), size: 30.h)
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: ()=> Get.bottomSheet(SelectAgeBottomSheetWidget(postController: controller)),
                        child: Container(
                          margin: EdgeInsets.only(top: 30.h),
                          padding: EdgeInsets.symmetric(vertical: 20.h),
                          width: double.infinity,
                          decoration: const BoxDecoration(
                              border: Border(bottom: BorderSide(color: Colors.blue, width: 1))
                          ),
                          child: Row(
                            children: [
                              const Icon(Icons.access_time_filled, color: Colors.blue,),
                              Expanded(child: Container(margin: EdgeInsets.only(left: 10.w), child: Text('age', style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300),))),
                              Text('${controller.agePet} year', style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300)),
                              Icon(Icons.arrow_forward_ios, color: Color(0xff727678), size: 30.h)
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: ()=> Get.bottomSheet(SelectTypePetBottomSheetWidget(postController: controller)),
                        child: Container(
                          margin: EdgeInsets.only(top: 30.h),
                          padding: EdgeInsets.symmetric(vertical: 20.h),
                          width: double.infinity,
                          decoration: const BoxDecoration(
                              border: Border(bottom: BorderSide(color: Colors.blue, width: 1))
                          ),
                          child: Row(
                            children: [
                              const Icon(Icons.access_time_filled, color: Colors.blue,),
                              Expanded(child: Container(margin: EdgeInsets.only(left: 10.w), child: Text('type pet', style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300),))),
                              Text(controller.typePet.value.typeName, style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300)),
                              Icon(Icons.arrow_forward_ios, color: Color(0xff727678), size: 30.h)
                            ],
                          ),
                        ),
                      ),
                      GetBuilder<PostController>(builder: (con){
                        return GestureDetector(
                          onTap: ()=> Get.toNamed(Routes.map)?.then((value) {
                            if(value is CurrentPositionEntity) controller.setLocation(value);
                          }),
                          child: Container(
                            margin: EdgeInsets.only(top: 30.h),
                            padding: EdgeInsets.symmetric(vertical: 20.h),
                            width: double.infinity,
                            decoration: const BoxDecoration(
                                border: Border(bottom: BorderSide(color: Colors.blue, width: 1))
                            ),
                            child: Row(
                              children: [
                                const Icon(Icons.access_time_filled, color: Colors.blue,),
                                Expanded(flex: 1,child: Container(margin: EdgeInsets.only(left: 10.w), child: Text('location', style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300),)),),
                                Expanded(child: Container(
                                  alignment: Alignment.centerRight,
                                  child: Column(
                                    children: [
                                      Text(con.currentPositionEntityObs.value?.addressName ?? 'specify the place', style: TextStyle(color: const Color(0xff727678), fontSize: 32.sp, fontWeight: FontWeight.w300), overflow: TextOverflow.ellipsis,),
                                    ],
                                  ),
                                )),
                                Icon(Icons.arrow_forward_ios, color: Color(0xff727678), size: 30.h)
                              ],
                            ),
                          ),
                        );
                      }),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.symmetric(vertical: 40.h, horizontal: 40.w),
          width: double.infinity,
          child: ElevatedButton(
            onPressed: ()=> controller.requestCreatePost(),
            style: ElevatedButton.styleFrom(
              primary: Colors.blue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
            ),
            child: Text('txt_confirm'.tr),
          ),
        ),
      ),
    ));
  }

  List<Widget> _listPictureWidget(){
    List<Widget> widgetList = [];
    widgetList.add(
      SizedBox(
        width: 260.w,
        height: 260.w,
        child: Stack(
          alignment: Alignment.center,
          children: [
            GestureDetector(
              onTap: ()=> controller.showActionSheetImage(),
              child: Container(
                alignment: Alignment.center,
                width: 200.w,
                height: 200.w,
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(16.r)
                ),
                child: Icon(Icons.add_a_photo_rounded, size: 100.w, color: Colors.white,),
              ),
            )
          ],
        ),
      )
    );

    for(String item in controller.listPicture){
      widgetList.add(
        SizedBox(
          width: 260.w,
          height: 260.w,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 200.w,
                width: 200.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16.r),
                    image: DecorationImage(
                        image: FileImage(File(item)), fit: BoxFit.cover)
                ),
              ),
              Positioned(
                top: 5,
                right: 5.0,
                child: GestureDetector(
                  onTap: ()=> controller.removePicture(item),
                  child: Container(
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.red,
                    ),
                    child: const Icon(Icons.close, color: Colors.white,),
                  ),
                ),
              )
            ],
          ),
        )
      );
    }

    return widgetList;
  }
}