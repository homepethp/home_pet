import 'package:flutter/material.dart';

import '../../../config/app_colors.dart';

class ImageError extends StatelessWidget {
  const ImageError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Icon(Icons.error, color: AppColors.primary);
  }
}