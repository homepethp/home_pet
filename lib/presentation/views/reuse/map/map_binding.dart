import 'package:get/get.dart';
import 'package:home_pet/presentation/views/reuse/map/map_controller.dart';

class MapBinding extends Bindings{

  @override
  void dependencies() {
    Get.put<MapController>(MapController());
  }
}