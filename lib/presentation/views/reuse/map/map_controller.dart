// ignore_for_file: unnecessary_null_comparison

import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/data/enum/app_state.dart';
import 'package:home_pet/data/models/place_search_model.dart';
import 'package:home_pet/domain/entities/current_position_entity.dart';
import 'package:home_pet/domain/entities/place_search_entity.dart';
import 'package:home_pet/domain/usecases/get/get_current_location.dart';
import 'package:home_pet/domain/usecases/get/get_location_by_keyword.dart';
import 'package:home_pet/domain/usecases/patch/patch_place_search.dart';
import 'package:home_pet/presentation/widgets/reuse/input_reuse_widget.dart';

class MapController extends GetxController with InputWidgetController{

  final appState = AppState.initial.obs;
  final GetCurrentLocation _getCurrentLocation = Get.find<GetCurrentLocation>();
  final GetLocationByKeyword _getLocationByKeyword = Get.find<GetLocationByKeyword>();
  final PatchPlaceSearch _patchPlaceSearch = Get.find<PatchPlaceSearch>();

  final Completer<GoogleMapController> _mapController = Completer();
  final double _mapZoom = 16;
  CameraPosition? _kGooglePlex;
  CurrentPositionEntity? _currentPositionEntityObs;
  final Map<MarkerId, Marker> _markers = <MarkerId, Marker>{}.obs;
  final RxString _detailLocation = ''.obs;
  final TextEditingController _searchLocationInputController = TextEditingController();
  final RxList<PlaceSearchEntity> _listPlaceSearch = RxList([]);

  Rx<Completer<GoogleMapController>> get mapController => _mapController.obs;
  Rx<CameraPosition?> get kGooglePlex => _kGooglePlex.obs;
  Rx<CurrentPositionEntity?> get currentPositionEntityObs => _currentPositionEntityObs.obs;
  Map<MarkerId, Marker> get markers => _markers;
  RxString get detailLocation => _detailLocation;
  Rx<TextEditingController> get searchLocationInputController => _searchLocationInputController.obs;
  RxList<PlaceSearchEntity> get listPlaceSearch => _listPlaceSearch;

  @override
  void onInit() {
    _searchLocationInputController.addListener(getLocationByKeyword);
    getCurrentLocation();
   super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getCurrentLocation({bool isCurrent = false}) async {
    if(!isCurrent) appState.value = AppState.loading;

    AppResult<CurrentPositionEntity> appResult = await _getCurrentLocation.call(NoParams());

    appResult.whenWithResult((result) async {
      _currentPositionEntityObs = result.value;
      _kGooglePlex = CameraPosition(target: result.value.latLng, zoom: _mapZoom);

      if(!isCurrent){
        _detailLocation.value = '${result.value.subDistrict} ${result.value.district} ${result.value.province}';

        String markerIdVal = result.value.addressName;
        final MarkerId markerId = MarkerId(markerIdVal);

        Marker marker = Marker(
          markerId: markerId,
          position: result.value.latLng,
          infoWindow: InfoWindow(title: markerIdVal),
          onTap: () {
            // _onMarkerTapped(markerId);
          },
        );

        _markers[markerId] = marker;

      }else{
        GoogleMapController controller = await _mapController.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: result.value.latLng, zoom: 16)));
      }

      update();
      appState.value = AppState.success;

    }, (error) {
      appState.value = AppState.error;
    });
  }

  void getLocationByKeyword() async {
    Timer(const Duration(milliseconds: 50), () async {
      if(_searchLocationInputController.text != ''){
        AppResult<List<PlaceSearchEntity>> appResult = await _getLocationByKeyword.call(_searchLocationInputController.text);

        appResult.whenWithResult((result) {
          _listPlaceSearch.value = [];
          for(PlaceSearchEntity item in result.value){
            _listPlaceSearch.add(item);
          }
        }, (error) {

        });
      }else{
        _listPlaceSearch.value = RxList([]);
      }
    });
  }

  void chooseFromPlaceSearch(PlaceSearchEntity data) async  {
    AppResult<CurrentPositionEntity> appResult = await _patchPlaceSearch.call(data);

    appResult.whenWithResult((result) async {
      _currentPositionEntityObs = result.value;
      _kGooglePlex = CameraPosition(target: result.value.latLng, zoom: _mapZoom);

        _detailLocation.value = '${result.value.subDistrict} ${result.value.district} ${result.value.province}';

        String markerIdVal = result.value.addressName;
        final MarkerId markerId = MarkerId(markerIdVal);

        Marker marker = Marker(
          markerId: markerId,
          position: result.value.latLng,
          infoWindow: InfoWindow(title: markerIdVal),
          onTap: () {
            // _onMarkerTapped(markerId);
          },
        );

        _markers.clear();
        _markers[markerId] = marker;

        GoogleMapController controller = await _mapController.future;
        controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(target: result.value.latLng, zoom: 16)));

    }, (error) {
    });
  }

  void clearInputPlaceSearch(){
    _searchLocationInputController.text = '';
  }
}