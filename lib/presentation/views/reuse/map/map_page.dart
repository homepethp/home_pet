import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:home_pet/data/enum/app_state.dart';
import 'package:home_pet/presentation/views/reuse/map/map_controller.dart';

import '../../../widgets/reuse/bottom_sheet_widget/search_location_bottom_sheet.dart';

class MapPage extends GetView<MapController>{
  const MapPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          onPressed: ()=> Get.back(),
          icon: Container(
            alignment: Alignment.center,
            width: 70.w,
            height: 70.w,
            decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.3),
              shape: BoxShape.circle
            ),
            child: Icon(Icons.arrow_back_ios_rounded, color: Colors.grey.shade700, size: 45.w,)),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: Obx((){
        switch(controller.appState.value){
          case AppState.initial:
          case AppState.loading : return Container();
          case AppState.success: return _widgetBuilder();
          case AppState.error: return Container();
        }
      }),
    );
  }

  Widget _widgetBuilder()=> Stack(
    children: [
      _mapBuilder(),
      _contentBuilder(),
    ],
  );

  Widget _mapBuilder()=> GoogleMap(
    initialCameraPosition: controller.kGooglePlex.value!,
    onMapCreated: (GoogleMapController mapController){
      controller.mapController.value.complete(mapController);
    },
    markers: Set<Marker>.of(controller.markers.values),
    myLocationButtonEnabled: false,
  );

  Widget _contentBuilder()=> SafeArea(
    top: false,
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: 40.w),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          _btnAction(const Icon(Icons.search_rounded, color: Colors.blue), ()=> {controller.clearInputPlaceSearch(), Get.bottomSheet(SearchLocationBottomSheet(mapController: controller))}),
          SizedBox(height: 20.h,),
          _btnAction(const Icon(Icons.my_location, color: Colors.blue,), ()=> controller.getCurrentLocation(isCurrent: true)),
          SizedBox(height: 20.h,),
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
           decoration: BoxDecoration(
             color: Colors.white,
             borderRadius: BorderRadius.circular(40.r),
             boxShadow: [
               BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 7,
                offset: const Offset(0, 1)
               )
             ]
           ),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      height: 120.w,
                      width: 120.w,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                      ),
                      child: ClipOval(
                        child: CachedNetworkImage(
                          imageUrl: 'https://ichef.bbci.co.uk/news/640/cpsprodpb/1124F/production/_119932207_indifferentcatgettyimages.png',
                          fit: BoxFit.cover,
                          placeholder: (context, _){
                            return const SpinKitCircle(
                              color: Colors.blue,
                            );
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 20.w),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(controller.currentPositionEntityObs.value!.addressName, style: TextStyle(color: Color(0xff003366), fontSize: 32.sp, fontWeight: FontWeight.w500), overflow: TextOverflow.ellipsis,),
                            Text(controller.detailLocation.value, style: TextStyle(color: Color(0xff003366), fontSize: 28.sp, fontWeight: FontWeight.w400), overflow: TextOverflow.ellipsis),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.w),
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                    onPressed: ()=> Get.back(result: controller.currentPositionEntityObs.value),
                    child: Text('txt_confirm'.tr, style: TextStyle(color: Colors.white, fontSize: 32.sp, fontWeight: FontWeight.w400),),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    ),
  );

  Widget _btnAction(Icon icon, VoidCallback callback){
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        width: 100.w,
        height: 100.w,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  blurRadius: 7,
                  offset: const Offset(0, 1)
              )
            ]
        ),
        child: IconButton(
          onPressed: callback,
          icon: icon,
        ),
      ),
    );
  }
}