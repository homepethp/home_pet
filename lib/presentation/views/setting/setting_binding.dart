import 'package:get/get.dart';
import 'package:home_pet/presentation/views/setting/setting_controller.dart';

class SettingBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<SettingController>(SettingController());
  }
}