import 'dart:async';
import 'package:get/get.dart';
import 'package:home_pet/core/result/app_result.dart';
import 'package:home_pet/core/usecases/usecase.dart';
import 'package:home_pet/domain/entities/data_user_login_entity.dart';
import 'package:home_pet/domain/usecases/local/get_data_user_login.dart';
import 'package:home_pet/domain/usecases/local/get_skip_recommend.dart';
import 'package:home_pet/domain/usecases/post/post_user_login.dart';
import 'package:home_pet/utils/app_permission_util.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../config/routes/app_routes.dart';
import '../../../data/request/login_request.dart';
import '../../../domain/entities/user_auth_entity.dart';

class SplashController extends GetxController{

  final GetSkipRecommend _getSkipRecommend = Get.find<GetSkipRecommend>();
  final GetDataUserLogin _getDataUserLogin = Get.find<GetDataUserLogin>();
  final PostUserLogin _postUserLogin = Get.find<PostUserLogin>();

  Timer? _timer;

  @override
  void onInit() {
    _getPermissionLocation();
    super.onInit();
  }

  @override
  void onClose() {
    _timer!.cancel();
    super.onClose();
  }

  Future<void> _getPermissionLocation() async {
    if (await AppPermissionUtil(context: Get.context!).isPermissionGranted(Permission.location)) {
      if (await Permission.location.serviceStatus.isEnabled) {
        getSkipRecommend();
      }
    }
  }

  void getSkipRecommend() async {
    bool resultSkip = await _getSkipRecommend.call(NoParams());
    if(resultSkip){
      AppResult<DataUserLoginEntity> appResultGetUserLogin = await _getDataUserLogin.call(NoParams());
      appResultGetUserLogin.whenWithResult((result) async {
        _loginFromUserSaveInLocal(result.value);
      }, (error) {
        _timer = Timer(const Duration(seconds: 2), ()=> Get.offNamedUntil(Routes.login, (route) => false));
      });
    }else{
      _timer = Timer(const Duration(seconds: 2), ()=> Get.offNamedUntil(Routes.recommend, (route) => false));
    }
  }

  void _loginFromUserSaveInLocal(DataUserLoginEntity data) async {
    LoginRequest params = LoginRequest(username: data.username, password: data.password);

    AppResult<UserAuthEntity> appResult = await _postUserLogin.call(params);

    appResult.whenWithResult((result) async {
      _timer = Timer(const Duration(seconds: 2), ()=> Get.offNamedUntil(Routes.dashBoard, (route) => false));
    }, (error) {

    });
  }
}