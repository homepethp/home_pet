import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'splash_controller.dart';

class SplashPage extends GetView<SplashController>{
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Image(image: AssetImage('assets/png/bg_home_pet.png'), fit: BoxFit.cover, width: double.infinity,),
    );
  }
}