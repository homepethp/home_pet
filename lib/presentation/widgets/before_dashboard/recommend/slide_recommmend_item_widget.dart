import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../data/models/slide_recommend_model.dart';

class SlideRecommendItemWidget extends StatelessWidget {
  final int index;
  final List<SlideRecommendModel> listSlide;

  const SlideRecommendItemWidget({Key? key, required this.index, required this.listSlide }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/png/bg_recommend.png'), fit: BoxFit.cover)
      ),
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image(image: AssetImage(listSlide[index].imageHead)),
            Image(image: AssetImage(listSlide[index].imageBody), height: 800.h,),
            Text(
              slideList[index].title,
              style: TextStyle(
                fontSize: 44.sp,
                color: const Color(0xff04385a),
                fontWeight: FontWeight.w600
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20.h,
            ),
            Text(
              listSlide[index].description,
              style: TextStyle(
                color: const Color(0xff010101),
                fontSize: 32.sp,
                fontWeight: FontWeight.w300
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
