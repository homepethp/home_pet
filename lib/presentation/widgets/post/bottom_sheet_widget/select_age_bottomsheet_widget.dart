import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/presentation/views/post/post_controller.dart';

import '../../../../config/app_colors.dart';

class SelectAgeBottomSheetWidget extends StatelessWidget {
  final PostController postController;
  const SelectAgeBottomSheetWidget({Key? key, required this.postController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(16.r), bottom: Radius.circular(16.r))
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Age', style: TextStyle(color: AppColors.fontPrimary, fontSize: 38.sp, fontWeight: FontWeight.w500),),
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: postController.basePetsEntity.listAgePet.length,
                itemBuilder: (context, index){
                  String value = postController.basePetsEntity.listAgePet[index];

                  return GestureDetector(
                    onTap: ()=> {postController.setAgePet(value), Get.back()},
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 20.h),
                      child: Row(
                        children: [
                          const Icon(Icons.access_time_filled, color: Colors.blue,),
                          Padding(
                            padding: EdgeInsets.only(left: 20.w),
                            child: Text('$value year', style: TextStyle(color: AppColors.fontPrimary, fontSize: 32.sp, fontWeight: FontWeight.w400),),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
