import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/app_colors.dart';
import 'package:home_pet/domain/entities/base_pets_entity.dart';
import 'package:home_pet/presentation/views/post/post_controller.dart';

class SelectSexBottomSheetWidget extends StatelessWidget {
  final PostController postController;

  const SelectSexBottomSheetWidget({Key? key, required this.postController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(16.r), bottom: Radius.circular(16.r))
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Sex', style: TextStyle(color: AppColors.fontPrimary, fontSize: 38.sp, fontWeight: FontWeight.w500),),
          ListView.builder(
              shrinkWrap: true,
              itemCount: postController.basePetsEntity.listSexPet.length,
              itemBuilder: (context, index){
                PetSexDataEntity value = postController.basePetsEntity.listSexPet[index];

                return GestureDetector(
                  onTap: ()=> {postController.setSexPet(value), Get.back()},
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: Row(
                      children: [
                        const Icon(Icons.access_time_filled, color: Colors.blue,),
                        Padding(
                          padding: EdgeInsets.only(left: 20.w),
                          child: Text(value.nameSex, style: TextStyle(color: AppColors.fontPrimary, fontSize: 32.sp, fontWeight: FontWeight.w400),),
                        )
                      ],
                    ),
                  ),
                );
              })
        ],
      ),
    );
  }
}
