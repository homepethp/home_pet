import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/domain/entities/base_pets_entity.dart';

import '../../../../config/app_colors.dart';
import '../../../views/post/post_controller.dart';

class SelectTypePetBottomSheetWidget extends StatelessWidget {
  final PostController postController;

  const SelectTypePetBottomSheetWidget({Key? key, required this.postController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(16.r), bottom: Radius.circular(16.r))
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('Type Pet', style: TextStyle(color: AppColors.fontPrimary, fontSize: 38.sp, fontWeight: FontWeight.w500),),
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: postController.basePetsEntity.listPetType.length,
                itemBuilder: (context, index){
                  PetTypeDataEntity value = postController.basePetsEntity.listPetType[index];

                  return GestureDetector(
                    onTap: ()=> {postController.setTypePet(value), Get.back()},
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 20.h),
                      child: Row(
                        children: [
                          Container(
                            width: 100.w,
                            height: 100.w,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: AppColors.primaryGradient(),
                                image: DecorationImage(
                                  image: AssetImage(value.pathImageType),
                                  fit: BoxFit.cover
                                )
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 20.w),
                            child: Text(value.typeName, style: TextStyle(color: AppColors.fontPrimary, fontSize: 32.sp, fontWeight: FontWeight.w400),),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
