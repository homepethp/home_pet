import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:home_pet/config/app_colors.dart';
import 'package:home_pet/presentation/views/reuse/map/map_controller.dart';

class SearchLocationBottomSheet extends StatelessWidget {
  final MapController mapController;

  const SearchLocationBottomSheet({Key? key, required this.mapController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(30.r))
      ),
      padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.w),
      child: Column(
        children: [
          mapController.inputReuseWidget(
              txtController: mapController.searchLocationInputController.value,
              hintText: 'search location',
              textInputType: TextInputType.text,
              txtColor: AppColors.fontPrimary,
              cursorColor: AppColors.fontPrimary,
              suffixIcon: Icon(Icons.search, color: Colors.grey, size: 50.w,)
          ),
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: mapController.listPlaceSearch.length,
                itemBuilder: (context, index){
                  return GestureDetector(
                    onTap: ()=> {Get.back(), mapController.chooseFromPlaceSearch(mapController.listPlaceSearch[index])},
                    child: Container(
                      margin: EdgeInsets.only(top: index == 0 ? 30.h : 0),
                      padding: EdgeInsets.symmetric(vertical: 20.h),
                      decoration: const BoxDecoration(
                        border: Border(
                          bottom: BorderSide(color: Colors.blue, width: 0.5)
                        )
                      ),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            width: 50.w,
                            height: 50.w,
                            decoration: const BoxDecoration(
                              color: Colors.blue,
                              shape: BoxShape.circle
                            ),
                            child: Icon(Icons.location_on, size: 35.w, color: Colors.white,),
                          ),
                          Expanded(
                              child: Padding(
                                  padding: EdgeInsets.only(left: 25.w),
                                  child: Text(mapController.listPlaceSearch[index].addressName,
                                    style: TextStyle(color: AppColors.fontPrimary, fontSize: 38.sp, fontWeight: FontWeight.w400),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  )
                              )
                          ),
                        ],
                      ),
                    ),
                  );
                }
            ),
          )
        ],
      ),
    ));
  }
}