import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SelectImageBottomSheet extends StatelessWidget {
  final VoidCallback firstCallback, secondCallback;

  const SelectImageBottomSheet({Key? key, required this.firstCallback, required this.secondCallback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 40.h, horizontal: 40.w),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.r)
      ),
      child: SafeArea(
        bottom: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            GestureDetector(
              onTap: firstCallback,
              child: Row(
                children: [
                  const Icon(Icons.camera_alt, color: Colors.blue),
                  Container(
                    margin: EdgeInsets.only(left: 20.w),
                    child: Text('Camera', style: TextStyle(fontSize: 34.sp, fontWeight: FontWeight.w400)))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30.h),
              child: GestureDetector(
                onTap: secondCallback,
                child: Row(
                  children: [
                    const Icon(Icons.image, color: Colors.blue),
                    Container(
                      margin: EdgeInsets.only(left: 20.w),
                      child: Text('Gallery', style: TextStyle(fontSize: 34.sp, fontWeight: FontWeight.w400)))
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
