// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class DialogButtonOneWidget extends StatelessWidget {
  String? title;
  String? description;

  DialogButtonOneWidget({Key? key, this.title, this.description}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.w)),
      child: _dialogViewBuilder(context),
    );
  }

  Widget _dialogViewBuilder(context) {
    return Container(
      padding: EdgeInsets.all(ScreenUtil().setWidth(40)),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(40.w)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(title ?? 'พบข้อผิดพลาด', style: TextStyle(color: Colors.black, fontSize: 32.sp, fontWeight: FontWeight.w500),),
          Container(
            margin: EdgeInsets.only(top: 20.h),
            child: Text(description!, style: TextStyle(color: Colors.black, fontSize: 28.sp, fontWeight: FontWeight.w300), textAlign: TextAlign.center,),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 40.h, bottom: 10.h),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16)
            ),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
              onPressed: ()=> Get.back(),
              child: Text('txt_close'.tr),
            ),
          ),
        ],
      ),
    );
  }
}