// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class LoadingDialogWidget extends StatelessWidget {
  String? message;

  LoadingDialogWidget({Key? key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.w)),
      child: _dialogViewBuilder(context),
    );
  }

  Widget _dialogViewBuilder(context) {
    return Container(
      padding: EdgeInsets.all(ScreenUtil().setWidth(40)),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(40.w)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              const SpinKitFadingCircle(
                color: Colors.blue,
              ),
              SizedBox(
                width: ScreenUtil().setWidth(40),
              ),
              Expanded(
                child: Text(
                  message == null
                      ? 'txt_loading'.tr
                      : message!,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 28.sp,
                      fontWeight: FontWeight.w400,
                  )
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}