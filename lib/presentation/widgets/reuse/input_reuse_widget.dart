import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InputWidgetController{

  Widget inputReuseWidget({
    TextEditingController? txtController,
    FocusNode? focusNode,
    TextInputType textInputType = TextInputType.text,
    String hintText = '',
    String? label,
    String? txtError,
    int? maxLength,
    int maxLines = 1,
    bool isPassword = false,
    bool isObscure = false,
    Function? switchObscurePassword,
    Color txtColor = Colors.white,
    Color txtLabelColor = Colors.white,
    Color txtLabelStyleColor = Colors.white,
    Color cursorColor = Colors.white,
    Widget? suffixIcon
  })=> TextFormField(
    focusNode: focusNode,
    controller: txtController,
    keyboardType: textInputType,
    maxLength: maxLength,
    maxLines: maxLines,
    obscureText: isPassword ? isObscure : false,
    style: TextStyle(fontSize: 32.sp, color: txtColor, fontWeight: FontWeight.w300),
    cursorColor: cursorColor,
    decoration: InputDecoration(
      suffixIcon: suffixIcon,
      counterText: '',
      errorText: txtError,
      hintText: hintText,
      hintStyle: TextStyle(fontSize: 32.sp, color: Colors.grey),
      label: label != null ? Text(label, style: TextStyle(fontSize: 26.sp, color: txtLabelColor, fontWeight: FontWeight.w400)) : null,
      labelStyle: TextStyle(fontSize: 26.sp, color: txtLabelStyleColor, fontWeight: FontWeight.w400),
      // suffixIcon: (isPassword) ? IconButton(
      //     icon: Icon(
      //       isObscure ? Icons.visibility : Icons.visibility_off,
      //     ),
      //     onPressed: () => switchObscurePassword!()
      // ) : null,
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: BorderSide(
            color: Colors.blue.shade800,
            width: 2.0
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: const BorderSide(
          color: Color(0xff66ccff),
          width: 2.0,
        ),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: const BorderSide(
          color: Colors.red,
          width: 2.0,
        ),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(16),
        borderSide: const BorderSide(
          color: Colors.red,
          width: 2.0,
        ),
      ),
    ),
  );
}