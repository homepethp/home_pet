// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../config/app_colors.dart';

class ReuseSilverAppbarWidget extends StatelessWidget {
  final String title;
  final bool innerBoxIsScrolled;
  List<Widget>? actions = [];
  Widget? leading;

  ReuseSilverAppbarWidget({Key? key, required this.title, required this.innerBoxIsScrolled, this.actions, this.leading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/png/icon_home_pet_circle_white.png', width: 100.w, height: 100.w,),
          Container(margin: EdgeInsets.only(left: 20.w), child: Text(title, style: TextStyle(color: Colors.white, fontSize: 48.sp, fontWeight: FontWeight.w500),))
        ],
      ),
      actions: actions,
      leading: leading ?? Container(),
      pinned: false,
      centerTitle: true,
      floating: true,
      forceElevated: innerBoxIsScrolled,
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: AppColors.primaryGradient(),
          boxShadow: const <BoxShadow>[
            BoxShadow(
                color: Colors.black54,
                blurRadius: 10.0,
                offset: Offset(0.0, 0.0)
            )
          ],
        ),
      ),
    );
  }
}
