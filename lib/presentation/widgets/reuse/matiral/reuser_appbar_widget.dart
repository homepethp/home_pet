// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../config/app_colors.dart';

class ReuseAppbarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  List<Widget>? actions;
  bool? isLogo = false;
  Widget? leading;

  ReuseAppbarWidget({Key? key, required this.title, this.actions, this.isLogo, this.leading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title, style: TextStyle(color: Colors.white, fontSize: 48.sp, fontWeight: FontWeight.w400),),
      actions: actions,
      leading: leading,
      centerTitle: true,
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: AppColors.primaryGradient()
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(56);
}
