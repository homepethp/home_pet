class AppInputUtils{

  bool checkEmailValidator(String txtEmail){
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+", caseSensitive: true).hasMatch(txtEmail);
    return emailValid;
  }

  bool checkPasswordValidator(String txtPassword){
    bool passwordValid = RegExp(r'^((?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{6,8})$', caseSensitive: true).hasMatch(txtPassword);
    return passwordValid;
  }

  bool checkTelValidator(String txtTel){
    bool telValid = RegExp(r'^(?:[+0]9)?[0-9]{10}$', caseSensitive: true).hasMatch(txtTel);
    return telValid;
  }
}