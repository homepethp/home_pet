import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class AppPermissionUtil{

  final BuildContext context;

  AppPermissionUtil({required this.context});

  Future<bool> isPermissionGranted(Permission permission) async {
    bool isAllPermissionGranted = true;
    if(Platform.isIOS){
      if(await permission.isDenied){
        isAllPermissionGranted = await _doPermissionRequest(permission);
      }else if(await permission.isPermanentlyDenied){
        isAllPermissionGranted = false;
      }
    }else{
      if(await permission.isDenied){
        isAllPermissionGranted = await _doPermissionRequest(permission);
      }
    }
    return isAllPermissionGranted;
  }

  Future<bool> _doPermissionRequest(Permission permission) async {
    PermissionStatus result = await permission.request();
    if(result == PermissionStatus.permanentlyDenied){
      return false;
    }else if(result == PermissionStatus.granted){
      return true;
    }else{
      return false;
    }
  }
}